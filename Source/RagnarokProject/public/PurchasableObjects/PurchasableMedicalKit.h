#pragma once

#include "CoreMinimal.h"
#include "PurchasableObject.h"
#include "PurchasableMedicalKit.generated.h"


UCLASS()
class RAGNAROKPROJECT_API APurchasableMedicalKit : public APurchasableObject
{
	GENERATED_BODY()

public:
	APurchasableMedicalKit();

	virtual void BeginPlay() override;
	void OnObjectPurchased(float Money) override;
	bool CheckSpecificPurchaseCondition() override;
};
