#pragma once

#include "CoreMinimal.h"
#include "PurchasableObject.h"
#include "PurchasableAmmoKit.generated.h"


UCLASS()
class RAGNAROKPROJECT_API APurchasableAmmoKit : public APurchasableObject
{
	GENERATED_BODY()

public:
	APurchasableAmmoKit();

	virtual void BeginPlay() override;
	void OnObjectPurchased(float Money) override;
	bool CheckSpecificPurchaseCondition() override;
};
