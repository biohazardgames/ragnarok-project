// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "PurchasableObject.h"
#include "PurchasableTurret.generated.h"


UCLASS()
class RAGNAROKPROJECT_API APurchasableTurret : public APurchasableObject
{
	GENERATED_BODY()

public:
	APurchasableTurret();
	virtual void BeginPlay() override;
	void OnObjectPurchased(float Money) override;

private:
	UStaticMeshComponent* BarrelsMesh;
	UMaterialInstanceDynamic* BarrelsMaterial;
};
