// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "PurchasableObjectsManager.h"
#include "Weapon.h"
#include "RagnarokProjectCharacter.h"
#include "PurchasableObject.generated.h"

class APurchasableObjectsManager;
class ARagnarokProjectCharacter;
class AWeapon;

UCLASS(Abstract)
class RAGNAROKPROJECT_API APurchasableObject : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
		USphereComponent * InteractionRadius;

	APurchasableObject();
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);
	float PurchaseObject(float Money);
	UFUNCTION()
		virtual void OnObjectPurchased(float Money);
	float GetEffectQuantity();
	UFUNCTION(BlueprintImplementableEvent)
		void OnNearPlayer(float Cost);

protected:
	UStaticMeshComponent * Mesh;
	UMaterialInstanceDynamic* Material;
	float Cost;
	int EffectQuantity;
	APurchasableObjectsManager* Manager;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* PurchasedSound;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* NotEnoughMoneySound;

	virtual void BeginPlay() override;
	void PlaySound(USoundBase* Sound);
	virtual bool CheckSpecificPurchaseCondition();
};
