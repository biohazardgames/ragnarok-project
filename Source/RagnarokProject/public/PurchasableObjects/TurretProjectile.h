#pragma once

#include "Projectile.h"
#include "TurretProjectile.generated.h"

class AAutomaticTurret;
class ABaseEnemy;

UCLASS()
class ATurretProjectile : public AProjectile
{
	GENERATED_BODY()

public:
	ATurretProjectile();

protected:
	void BeginPlay() override;
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;	
};