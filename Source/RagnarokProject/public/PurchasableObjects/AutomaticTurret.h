// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "AutomaticTurret.generated.h"

class ABaseEnemy;
class ATurretProjectile;

UCLASS()
class RAGNAROKPROJECT_API AAutomaticTurret : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Collision")
		USphereComponent* FireRadius;

	AAutomaticTurret();
	virtual void Tick(float DeltaTime) override;
	float GetDamage();
	void TargetKilled(uint32 TargetId);
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);

protected:
	virtual void BeginPlay() override;

private:
	UStaticMeshComponent* BaseMesh;
	UStaticMeshComponent* BarrelsMesh;
	UMaterialInstanceDynamic* BaseMaterial;
	UMaterialInstanceDynamic* BarrelsMaterial;
	TArray<AActor*> Targets;
	float FireRate;
	FTimerHandle FireTimer;
	class USoundBase* FireSound;
	class USoundBase* ArmedSound;
	float Damage;
	bool CanFire;

	void Fire();
	void LaunchProjectile(FName SocketName);
	void PlaySound(USoundBase* Sound);
	void DeleteTarget(uint32 TargetId);
};
