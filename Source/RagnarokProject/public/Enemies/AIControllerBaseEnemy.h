#pragma once

#include "CoreMinimal.h"
#include "EngineMinimal.h"
#include "EngineUtils.h"
#include "AIController.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "AIControllerBaseEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API AAIControllerBaseEnemy : public AAIController
{
	GENERATED_BODY()

public:
	UPROPERTY(transient)
		class UBlackboardComponent* BlackboardComp;
	UPROPERTY(transient)
		class UBehaviorTreeComponent* BehaviorTreeComp;
	uint8 PlayerKeyId;

	AAIControllerBaseEnemy();
	virtual void Possess(APawn* InPawn) override;
};