#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/BTService.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "RagnarokProjectCharacter.h"
#include "BTServiceBaseEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API UBTServiceBaseEnemy : public UBTService
{
	GENERATED_BODY()
	
public:
	UBTServiceBaseEnemy();
};