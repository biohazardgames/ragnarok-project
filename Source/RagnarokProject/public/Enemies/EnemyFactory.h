#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemyFactory.generated.h"

class ABaseEnemyFlyweight;
class AStandardEnemyFlyweight;
class ASorcererEnemyFlyweight;

UCLASS()
class RAGNAROKPROJECT_API AEnemyFactory : public AActor
{
	GENERATED_BODY()
	
public:	
	TMap<FString, ABaseEnemyFlyweight*> EnemyCommonStates;
	TMap<FString, TSubclassOf<ABaseEnemyFlyweight>> EnemyClasses;

	AEnemyFactory();
	ABaseEnemyFlyweight* GetCommonState(FString CommonStateName);

protected:
	virtual void BeginPlay() override;
};
