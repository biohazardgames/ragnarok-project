#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/HealthShieldComponent.h"
#include "EnemyHealthShieldComponent.generated.h"

class ABaseEnemy;

UCLASS()
class RAGNAROKPROJECT_API UEnemyHealthShieldComponent : public UHealthShieldComponent
{
	GENERATED_BODY()

public:	
	UEnemyHealthShieldComponent();

protected:
	virtual void BeginPlay() override;
	UFUNCTION()
		virtual void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser) override;
};