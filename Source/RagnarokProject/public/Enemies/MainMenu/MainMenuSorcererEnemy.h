#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainMenuSorcererEnemy.generated.h"

UCLASS()
class RAGNAROKPROJECT_API AMainMenuSorcererEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	AMainMenuSorcererEnemy();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Devouring;

protected:
	virtual void BeginPlay() override;

private:
	UClass* AnimationBlueprint;
};
