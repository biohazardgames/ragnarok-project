#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "MainMenuStandardEnemy.generated.h"

UCLASS()
class RAGNAROKPROJECT_API AMainMenuStandardEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	AMainMenuStandardEnemy();
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
		bool Devouring;

protected:
	virtual void BeginPlay() override;

private:
	UClass* AnimationBlueprint;
};
