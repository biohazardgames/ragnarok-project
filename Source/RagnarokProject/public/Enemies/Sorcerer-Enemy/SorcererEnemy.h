#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "GameFramework/Character.h"
#include "SorcererEnemy.generated.h"

class ASorcererEnemyFlyweight;
class ASorcererProjectile;

UCLASS()
class RAGNAROKPROJECT_API ASorcererEnemy : public ABaseEnemy
{
	GENERATED_BODY()

public:
	ASorcererEnemy();
	UFUNCTION(BlueprintCallable, Category = "Attack")
		void LaunchProjectile();

protected:
	virtual void BeginPlay() override;

private:
	UPROPERTY(VisibleDefaultsOnly, Category = "Projectile")
		FName ProjectileSocketName;
	FTimerHandle LaunchProjectileTimer;
};
