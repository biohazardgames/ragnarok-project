#pragma once

#include "Projectile.h"
#include "SorcererProjectile.generated.h"

class ASorcererEnemy;

UCLASS()
class ASorcererProjectile : public AProjectile
{
	GENERATED_BODY()

public:
	ASorcererProjectile();

protected:
	void BeginPlay() override;
	void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit) override;	
};