#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTaskBaseEnemy.h"
#include "BTTaskAttackSorcererEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API UBTTaskAttackSorcererEnemy : public UBTTaskBaseEnemy
{
	GENERATED_BODY()

public: 
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
