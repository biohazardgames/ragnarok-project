#pragma once

#include "CoreMinimal.h"
#include "BTServiceBaseEnemy.h"
#include "BTServiceSorcererEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API UBTServiceSorcererEnemy : public UBTServiceBaseEnemy
{
	GENERATED_BODY()
	
public:
	UBTServiceSorcererEnemy();
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
