#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseEnemyFlyweight.h"
#include "SorcererEnemyFlyweight.generated.h"

UCLASS()
class RAGNAROKPROJECT_API ASorcererEnemyFlyweight : public ABaseEnemyFlyweight
{
	GENERATED_BODY()

public:
	ASorcererEnemyFlyweight();

protected:
	virtual void BeginPlay() override;
};