#pragma once

#include "CoreMinimal.h"
#include "AIControllerBaseEnemy.h"
#include "AIControllerSorcererEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API AAIControllerSorcererEnemy : public AAIControllerBaseEnemy
{
	GENERATED_BODY()

public:
	AAIControllerSorcererEnemy();
	virtual void Possess(APawn* InPawn) override;
};