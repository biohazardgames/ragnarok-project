#pragma once

#include "CoreMinimal.h"
#include "EngineMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "EnemyPortalFlyweight.generated.h"

class AEnemyManager;
class ABaseEnemy;
class AStandardEnemy;
class ASorcererEnemy;

UCLASS()
class RAGNAROKPROJECT_API AEnemyPortalFlyweight : public AActor
{
	GENERATED_BODY()

public:
	AEnemyPortalFlyweight();
	AEnemyManager* GetManager();
	TSubclassOf<ABaseEnemy> GetEnemyClass(int EnemyClassKey);
	TSubclassOf<ABaseEnemy> GetBossClass(int BossClassKey);
	int GetNumberOfEnemyTypes();
	int GetNumberOfBossTypes();
	
protected:
	virtual void BeginPlay() override;
	
private:
	AEnemyManager* Manager;
	TMap<int, TSubclassOf<ABaseEnemy>> EnemyClasses;
	TMap<int, TSubclassOf<ABaseEnemy>> BossClasses;

	
};
