#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "EnemyPortal.generated.h"

class UStaticMeshComponent;
class AEnemyManager;
class AEnemyPortalFlyweight;
class ABaseEnemy;
class AStandardEnemy;
class ASorcererEnemy;

UCLASS()
class AEnemyPortal : public AActor
{
	GENERATED_BODY()

public:
	AEnemyPortal();
	void StartGame();
	void SpawnEnemy(bool IsBossWave);
	
protected:	
	virtual void BeginPlay() override;

private:
	UPROPERTY()
		UStaticMeshComponent* Mesh;
	UPROPERTY()
		TWeakObjectPtr<UParticleSystem> PortalFX;
	float DistanceToPortal;
	int LastEnemyTypeSpawnedKey;
	AEnemyPortalFlyweight* CommonState;
		
	void PlaySound(USoundBase* Sound);
};

