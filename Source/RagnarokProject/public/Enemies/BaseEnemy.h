#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Character.h"
#include "RagnarokProject.h"
#include "EnemyHealthShieldComponent.h"
#include "BaseEnemy.generated.h"

class ABaseEnemyFlyweight;
class AEnemyFactory;
class ARagnarokProjectCharacter;
class UEnemyHealthShieldComponent;
class ATurretProjectile;
class AAutomaticTurret;

UCLASS()
class RAGNAROKPROJECT_API ABaseEnemy : public ACharacter
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, Category = Behavior)
		class UBehaviorTree* Behavior;

	ABaseEnemy();
	UFUNCTION(Category = "Attack")
		virtual void Attack(AActor* Target);
	UFUNCTION(Category = "Death")
		void Death(AActor* DamageCauser);
	UFUNCTION(Category = "Death")
		virtual void DeathFinished();
	virtual float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;
	UBehaviorTree* GetBehavior();
	ABaseEnemyFlyweight* GetCommonState();
	UFUNCTION(BlueprintCallable, Category = "Attack")
		bool GetCanAttack();
	UFUNCTION(BlueprintCallable)
		bool GetIsNearPlayer();
	UFUNCTION(Category = "Attack")
		virtual void EnableCanAttack();
	UFUNCTION(BlueprintImplementableEvent)
		void OnChangeHealthShield(float Health, float Shield);

protected:
	ABaseEnemyFlyweight* CommonState;
	AEnemyFactory* Factory;
	FTimerHandle Timer;
	bool CanAttack;
	bool IsNearPlayer;
	bool Dead;
	UPROPERTY()
		UEnemyHealthShieldComponent* HealthShield;
	
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	UFUNCTION()
		void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);
	float PlayAnimation(UAnimMontage* Animation, float InPlayRate = 1.f, FName StartSectionName = NAME_None);
	void PlaySound(USoundBase* Sound);
};
