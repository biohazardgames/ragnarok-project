#pragma once

#include "CoreMinimal.h"
#include "EngineMinimal.h"
#include "GameFramework/Actor.h"
#include "UObject/ConstructorHelpers.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "BaseEnemyFlyweight.generated.h"

class ARagnarokProjectCharacter;
class AEnemyManager;

UCLASS()
class RAGNAROKPROJECT_API ABaseEnemyFlyweight : public AActor
{
	GENERATED_BODY()
	
	const float DEFAULT_MIN_DISTANCE_TO_PLAYER = 10.f;

public:
	ABaseEnemyFlyweight();
	ARagnarokProjectCharacter* GetPlayer();
	AEnemyManager* GetEnemyManager();
	float GetDamage();
	float GetEarnedMoney();
	float GetAttackCooldown();
	float GetSpeed();
	USoundBase* GetRunningSound();
	USoundBase* GetAttackSound();
	USoundBase* GetDeathSound();
	USoundBase* GetLaunchProjectileSound();
	float GetMinDistanceToPlayer(FString Class);
	UClass* GetAnimationBlueprint();
	TArray<UAnimMontage*> GetAttackAnimations();
	TArray<UAnimMontage*> GetDeathAnimations();

protected:
	float Damage;
	float EarnedMoney;
	float AttackCooldown;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* RunningSound;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* AttackSound;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* DeathSound;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* LaunchProjectileSound;
	ARagnarokProjectCharacter* Player;
	AEnemyManager* EnemyManager;
	UClass* AnimationBlueprint;
	UPROPERTY()
		TArray<UAnimMontage*> AttackAnimations;
	UPROPERTY()
		TArray<UAnimMontage*> DeathAnimations;
	float Speed;
	TMap<FString, float> EnemyMinDistanceToPlayer;

	virtual void BeginPlay() override;
};
