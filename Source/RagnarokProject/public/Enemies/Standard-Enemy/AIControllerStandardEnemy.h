#pragma once

#include "CoreMinimal.h"
#include "AIControllerBaseEnemy.h"
#include "AIControllerStandardEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API AAIControllerStandardEnemy : public AAIControllerBaseEnemy
{
	GENERATED_BODY()

public:
	AAIControllerStandardEnemy();

	virtual void Possess(APawn* InPawn) override;
};