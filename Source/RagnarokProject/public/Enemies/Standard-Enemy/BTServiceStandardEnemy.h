#pragma once

#include "CoreMinimal.h"
#include "BTServiceBaseEnemy.h"
#include "BTServiceStandardEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API UBTServiceStandardEnemy : public UBTServiceBaseEnemy
{
	GENERATED_BODY()
	
public:
	UBTServiceStandardEnemy();
	virtual void TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds) override;
};
