#pragma once

#include "CoreMinimal.h"
#include "BaseEnemy.h"
#include "GameFramework/Character.h"
#include "StandardEnemy.generated.h"

class AStandardEnemyFlyweight;

UCLASS()
class RAGNAROKPROJECT_API AStandardEnemy : public ABaseEnemy
{
	GENERATED_BODY()

public:
	AStandardEnemy();
	void Attack(AActor* Target) override;

protected:
	virtual void BeginPlay() override;
};
