#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BTTaskBaseEnemy.h"
#include "BTTaskAttackStandardEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API UBTTaskAttackStandardEnemy : public UBTTaskBaseEnemy
{
	GENERATED_BODY()

public: 
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
