#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "BaseEnemyFlyweight.h"
#include "StandardEnemyFlyweight.generated.h"

UCLASS()
class RAGNAROKPROJECT_API AStandardEnemyFlyweight : public ABaseEnemyFlyweight
{
	GENERATED_BODY()

public:
	AStandardEnemyFlyweight();

protected:
	virtual void BeginPlay() override;
};