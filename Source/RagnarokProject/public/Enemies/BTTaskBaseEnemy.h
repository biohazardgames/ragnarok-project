#pragma once

#include "CoreMinimal.h"
#include "BehaviorTree/Tasks/BTTask_BlackboardBase.h"
#include "BehaviorTree/BehaviorTree.h"
#include "BehaviorTree/BehaviorTreeComponent.h"
#include "BehaviorTree/BlackboardComponent.h"
#include "BehaviorTree/Blackboard/BlackboardKeyAllTypes.h"
#include "RagnarokProjectCharacter.h"
#include "BaseEnemyFlyweight.h"
#include "BTTaskBaseEnemy.generated.h"


UCLASS()
class RAGNAROKPROJECT_API UBTTaskBaseEnemy : public UBTTask_BlackboardBase
{
	GENERATED_BODY()

public: 
	virtual EBTNodeResult::Type ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory) override;
};
