// Copyright 1998-2017 Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "TimerManager.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"
#include "GameFramework/Character.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "PlayerHealthShieldComponent.h"
#include "RagnarokProjectCharacter.generated.h"

class AWeapon;
class AAssaultRifle;
class ALaserCannon;
class APurchasableObject;
class APurchasableObjectsManager;
class AEnemyManager;
class UPlayerHealthShieldComponent;
class HUDManager;
class APauseMenuManager;
class ABaseSkill;
class HealthFontSkill;
class AGameplayManager;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateMoneySignature, float, Money);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FOnUpdateAmmoSignature, int, AmmoInClip, int, CurrentAmmo);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateSkillStateSignature, bool, CanUseSkill);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnDiePlayerSignature);

UCLASS(config=Game)
class ARagnarokProjectCharacter : public ACharacter
{
	GENERATED_BODY()

	/** Camera boom positioning the camera behind the character */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class USpringArmComponent* CameraBoom;

	/** Follow camera */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category = Camera, meta = (AllowPrivateAccess = "true"))
	class UCameraComponent* FollowCamera;

public:
	ARagnarokProjectCharacter();

	/** Base turn rate, in deg/sec. Other scaling may affect final turn rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseTurnRate;
	/** Base look up/down rate, in deg/sec. Other scaling may affect final rate. */
	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	float BaseLookUpRate;
	UPROPERTY()
		AWeapon* CurrentWeapon;
	UPROPERTY()
		AWeapon* BackWeapon;
	UPROPERTY()
		TSubclassOf<ABaseSkill> CurrentSkill;
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* EquipAnimation;
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* DeathAnimation;
	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
		FName WeaponAttachSocketName;
	UPROPERTY(VisibleDefaultsOnly, Category = "Player")
		FName BackWeaponAttachSocketName;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnUpdateMoneySignature OnUpdateMoney;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnUpdateAmmoSignature OnUpdateAmmo;	
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnUpdateSkillStateSignature OnUpdateSkillState;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnDiePlayerSignature OnDiePlayer;
	
	UFUNCTION(BlueprintCallable)
		bool GetCrouchKeyPressed();
	UFUNCTION(BlueprintCallable)
		bool GetReloadKeyPressed();
	UFUNCTION(BlueprintCallable)
		bool GetSprintKeyPressed();
	UFUNCTION(BlueprintCallable)
		bool GetAimKeyPressed();
	UFUNCTION(BlueprintCallable)
		bool GetFireKeyPressed();
	/* Retrieve Pitch/Yaw from current camera */
	UFUNCTION(BlueprintCallable, Category = "Targeting")
		FRotator GetAimOffsets() const;
	UFUNCTION(BlueprintCallable)
		int getCurrentWeaponSlot();
	AWeapon* GetCurrentWeapon();
	void AmmoUpdated(int AmmoInClip, int CurrentAmmo);
	UPlayerHealthShieldComponent* GetHealthShieldComponent();
	UFUNCTION()
		void UpdateMoney(float Money);
	UFUNCTION()
		void PurchasedAmmo(int PurchasedAmmo);
	void SetPurchasableObject(APurchasableObject* PurchasableObject);
	void SetCanFire(bool CanFire);	
	void SetCanReload(bool CanReload);
	void SetCanAim(bool CanAim);
	void SetCanSwapWeapons(bool CanSwapWeapons);
	float TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser) override;
	UFUNCTION()
		void StartGame();

protected:

	/** Resets HMD orientation in VR. */
	void OnResetVR();
	
	/** Called when Crouch Key has been pressed */
	void Crouch();

	/** Called when Crouch Key has been released */
	void StopCrouching();

	/** Called when Reload Key has been pressed */
	void Reload();

	/** Called when Reload Key has been released */
	void StopReloading();

	/** Called when Sprint Key has been pressed */
	void Sprint();

	/** Called when Sprint Key has been released */
	void StopSprinting();

	/** Called when Aim Key has been pressed */
	void Aim();

	/** Called when Aim Key has been released */
	void StopAiming();

	/** Called when Fire Key has been pressed */
	void Fire();

	/** Called when Fire Key has been released */
	void StopFiring();

	/** Called when Interact Key has been pressed */
	void Interact();

	/** Called when Skill Key has been pressed */
	void UseSkill();

	/** Called when PrimaryWeapon Key has been pressed */
	void EquipPrimaryWeapon();

	/** Called when SecondaryWeapon Key has been pressed */
	void EquipSecondaryWeapon();

	/** Called when Pause Key has been pressed */
	void Pause();
	
	/** Called for forwards/backward input */
	void MoveForward(float Value);

	/** Called for side to side input */
	void MoveRight(float Value);

	/** 
	 * Called via input to turn at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void TurnAtRate(float Rate);

	/**
	 * Called via input to turn look up/down at a given rate. 
	 * @param Rate	This is a normalized rate, i.e. 1.0 means 100% of desired turn rate
	 */
	void LookUpAtRate(float Rate);

	/** Handler for when a touch input begins. */
	void TouchStarted(ETouchIndex::Type FingerIndex, FVector Location);

	/** Handler for when a touch input stops. */
	void TouchStopped(ETouchIndex::Type FingerIndex, FVector Location);

protected:
	// APawn interface
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;
	// End of APawn interface

public:
	virtual void BeginPlay() override;
	// Called every frame
	virtual void Tick(float DeltaTime) override;
	virtual FVector GetPawnViewLocation() const override;
	/** Returns CameraBoom subobject **/
	FORCEINLINE class USpringArmComponent* GetCameraBoom() const { return CameraBoom; }
	/** Returns FollowCamera subobject **/
	FORCEINLINE class UCameraComponent* GetFollowCamera() const { return FollowCamera; }

	/* Update the weapon mesh to the newly equipped weapon, this is triggered during an anim montage.
	Requires an AnimNotify created in the Equip animation to tell us when to swap the meshes. */
	UFUNCTION(BlueprintCallable, Category = "Animation")
		void SwapWeaponMeshes();
	void Death();
	void StartSkillCooldown(float CooldownTime);

private:
	bool CrouchKeyPressed;
	bool ReloadKeyPressed;
	bool SprintKeyPressed;
	bool AimKeyPressed;
	bool FireKeyPressed;
	bool CanFire;	
	bool CanReload;
	bool CanAim;
	bool CanSwapWeapons;
	float DefaultFOV;
	float ZoomedFOV;
	float ZoomInterpSpeed;
	UClass* AnimationBlueprint;
	APurchasableObject* PurchasableObject;
	float Money;
	int CurrentWeaponSlot;
	FTimerHandle Timer;
	FTimerHandle SkillTimer;
	USoundCue* HurtSound;
	APauseMenuManager* PauseMenuManager;
	UPROPERTY()
		UPlayerHealthShieldComponent* HealthShield;
	float BaseSpeed;
	float SpeedModifier;
	bool CanUseSkill;
	
	void SwapWeapons();
	void SwapWeaponsFinished();
	void PlayCharacterSound(USoundCue* SoundToPlay);
	void EnableCanUseSkill();
};