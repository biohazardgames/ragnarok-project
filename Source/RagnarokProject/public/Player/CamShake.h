#pragma once

#include "CoreMinimal.h"
#include "Camera/CameraShake.h"
#include "CamShake.generated.h"

UCLASS()
class RAGNAROKPROJECT_API UCamShake : public UCameraShake
{
	GENERATED_BODY()
	
public:
	UCamShake();	
	
};
