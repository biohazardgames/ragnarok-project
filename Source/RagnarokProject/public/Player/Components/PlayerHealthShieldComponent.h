#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "Components/HealthShieldComponent.h"
#include "PlayerHealthShieldComponent.generated.h"

class ARagnarokProjectCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_ThreeParams(FOnUpdateHealthShieldSignature, float, Health, float, Shield, bool, PlayerDamaged);

UCLASS()
class RAGNAROKPROJECT_API UPlayerHealthShieldComponent : public UHealthShieldComponent
{
	GENERATED_BODY()

public:	
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnUpdateHealthShieldSignature OnUpdateHealthShield;
		
	UPlayerHealthShieldComponent();
	virtual void Heal(float HealAmount) override;

protected:
	virtual void BeginPlay() override;
	UFUNCTION()
		virtual void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser) override;
};