#pragma once

#include "CoreMinimal.h"
#include "EngineMinimal.h"
#include "EngineUtils.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Components/SphereComponent.h"
#include "RagnarokProject.h"
#include "RagnarokProjectCharacter.h"
#include "BaseSkill.generated.h"

class ARagnarokProjectCharacter;

UCLASS(Abstract)
class ABaseSkill : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY()
		USphereComponent * InteractionRadius;

	ABaseSkill();
	UFUNCTION()
		virtual void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
		virtual void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
			int32 OtherBodyIndex);
	
protected:
	UPROPERTY()
		UStaticMeshComponent* Mesh;
	UPROPERTY()
		TWeakObjectPtr<UParticleSystem> SkillFX;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* SpawnedSound;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* EffectSound;
	float EffectQuantity;
	float CooldownTime;
	FTimerHandle CooldownTimer;
	AActor* SkillTarget;
	float TimeToFireSkill;
	int Charges;
		
	virtual void BeginPlay() override;
	virtual void FireSkill() PURE_VIRTUAL(ABaseSkill::FireSkill, );
	
private:
	void PlaySound(USoundBase* Sound);
};

