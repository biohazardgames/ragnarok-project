#pragma once

#include "BaseSkill.h"
#include "HealthFontSkill.generated.h"

class AHealthFontSkill;

UCLASS()
class AHealthFontSkill : public ABaseSkill
{
	GENERATED_BODY()

public:
	AHealthFontSkill();

protected:
	void BeginPlay() override;	
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) override;
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex) override;
	void FireSkill() override;
};