#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "LaserCannon.generated.h"

UCLASS()
class RAGNAROKPROJECT_API ALaserCannon : public AWeapon
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	ALaserCannon();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};
