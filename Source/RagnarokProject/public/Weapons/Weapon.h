#pragma once

#include "CoreMinimal.h"
#include "EngineMinimal.h"
#include "EngineUtils.h"
#include "GameFramework/Actor.h"
#include "PhysicsEngine/PhysicsAsset.h"
#include "RagnarokProjectCharacter.h"
#include "Runtime/Engine/Classes/Sound/SoundCue.h"
#include "TimerManager.h"
#include "CamShake.h"
#include "Weapon.generated.h"

UCLASS(Abstract)
class RAGNAROKPROJECT_API AWeapon : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AWeapon();	
	virtual void StartFire();
	virtual void StopFire();
	virtual void StartReload();
	virtual void SetOwner(AActor* NewOwner) override;
	void UpdateAmmo(int ClipAmmo, int Ammo);
	int GetAmmoInClip();
	int GetCurrentAmmo();
	int GetMaxAmmo();
	void SetBulletSpreadByPlayerPosition(FString Position);

protected:	
	int AmmoInClip;
	int ClipSize;
	int CurrentAmmo;
	int MaxAmmo;
	float Damage;
	float BulletSpread;
	TMap<FString, float> BulletSpreadByPlayerPosition;
	float TimeBetweenShots;
	float MaxShotDistance;
	USoundCue* FireSound;
	USoundCue* ReloadSound;	
	UMaterialInstanceDynamic* Material0;
	UMaterialInstanceDynamic* Material1;
	UPROPERTY(Category = Mesh, VisibleDefaultsOnly, BlueprintReadOnly, meta = (AllowPrivateAccess = "true"))
		class USkeletalMeshComponent* Mesh;	
	UPROPERTY(EditDefaultsOnly, Category = "Animation")
		UAnimMontage* ReloadAnimation;
	UPROPERTY(EditDefaultsOnly)
		FName MuzzleAttachSocketName;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* MuzzleFX;
	UPROPERTY(EditDefaultsOnly, BlueprintReadOnly)
		UParticleSystem* TracerEffect;

	float PlayWeaponAnimation(UAnimMontage* Animation, float InPlayRate = 1.f, FName StartSectionName = NAME_None);
	void StopWeaponAnimation(UAnimMontage* Animation);
	void PlayWeaponSound(USoundCue* SoundToPlay);

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	virtual void ReloadFinished();
	virtual void Fire();

private:
	float LastFireTime;
	FTimerHandle FireTimer;
	FTimerHandle ReloadTimer;
	ARagnarokProjectCharacter* Player;

	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* DefaultImpactEffect;
	UPROPERTY(EditDefaultsOnly)
		UParticleSystem* EnemyImpactEffect;
	UPROPERTY(VisibleDefaultsOnly, Category = "Weapon")
		FName TracerTargetName;
	UPROPERTY(EditDefaultsOnly)
		TSubclassOf<class UCameraShake> FireCamShake;

	void PlayFireEffects(FVector TraceEnd);
	void PlayImpactEffects(FHitResult Hit);
};