#pragma once

#include "CoreMinimal.h"
#include "Weapon.h"
#include "AssaultRifle.generated.h"

UCLASS()
class RAGNAROKPROJECT_API AAssaultRifle : public AWeapon
{
	GENERATED_BODY()
	
public:
	// Sets default values for this actor's properties
	AAssaultRifle();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
};
