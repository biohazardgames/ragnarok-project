#pragma once

#include "BaseManager.h"
#include "GameplayManager.generated.h"

class AEnemyManager;
class APurchasableObjectsManager;
class HUDManager;
class ARagnarokProjectCharacter;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnStartWaveSignature, int, Wave);
DECLARE_DYNAMIC_MULTICAST_DELEGATE(FOnStartGameSignature);

UCLASS()
class RAGNAROKPROJECT_API AGameplayManager : public ABaseManager
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnStartGameSignature OnStartGame;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnStartWaveSignature OnStartWave;
	
	AGameplayManager();
	UFUNCTION()
		void EnemyKilled(int RemainingEnemies);
	UFUNCTION()
		void GameOver();

protected:
	virtual void BeginPlay() override;

private:
	int Wave;
	float TimeToStartWave;

	void InitializeGame();
	void StartGame();
	void StartWave();
	void SetWaveTimer();
};
