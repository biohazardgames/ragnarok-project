#pragma once

#include "BaseManager.h"
#include "CreditsMenuManager.generated.h"

UCLASS()
class RAGNAROKPROJECT_API ACreditsMenuManager : public ABaseManager
{
	GENERATED_BODY()

public:
	ACreditsMenuManager();	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> CreditsMenuWidget;
	TWeakObjectPtr<class UUserWidget> pCreditsMenuWidget;	

protected:
	virtual void BeginPlay() override;	
};