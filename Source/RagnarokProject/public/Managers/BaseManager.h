#pragma once

#include "CoreMinimal.h"
#include "TextWidgetTypes.h"
#include "GameFramework/Actor.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "Blueprint/UserWidget.h"
#include "UMG.h"
#include "Runtime/UMG/Public/Blueprint/WidgetBlueprintLibrary.h"
#include "BaseManager.generated.h"

class AGameplayManager;

UCLASS()
class RAGNAROKPROJECT_API ABaseManager : public AActor
{
	GENERATED_BODY()
	
public:	
	ABaseManager();	
	UFUNCTION(BlueprintCallable)
		virtual void PlayManagerSound(USoundBase* SoundToPlay);
	UFUNCTION(BlueprintCallable)
		virtual void OnHoveredButton();
	UFUNCTION(BlueprintCallable)
		virtual void SwapLevel(FString levelName);
	UFUNCTION(BlueprintCallable)
		void QuitGameButtonClicked();

protected:
	FTimerHandle Timer;
	USoundBase*	AmbientalSong;
	AGameplayManager* GameplayManager;

	virtual void BeginPlay() override;	

private:
	USoundBase* ButtonHoveredSound;
};