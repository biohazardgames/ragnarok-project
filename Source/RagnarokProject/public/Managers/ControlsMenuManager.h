#pragma once

#include "BaseManager.h"
#include "ControlsMenuManager.generated.h"

UCLASS()
class RAGNAROKPROJECT_API AControlsMenuManager : public ABaseManager
{
	GENERATED_BODY()

public:
	AControlsMenuManager();	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> ControlsMenuWidget;
	TWeakObjectPtr<class UUserWidget> pControlsMenuWidget;	

protected:
	virtual void BeginPlay() override;	
};