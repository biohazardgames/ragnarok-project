
#pragma once

#include "GameFramework/SaveGame.h"
#include "RagnarokProjectSaveGame.generated.h"

/*It stores killed enemies and waves*/
USTRUCT(BlueprintType)
    struct FRecord 
	{         
        GENERATED_USTRUCT_BODY()
		
        UPROPERTY(EditAnywhere, BlueprintReadWrite)
			int KilledEnemies;
		UPROPERTY(EditAnywhere, BlueprintReadWrite)
			int Waves;
     
		FRecord() 
         : KilledEnemies(0), 
           Waves(0)
		{}
		
		int GetKilledEnemies(){
			return this->KilledEnemies;
        }
		
		void SetKilledEnemies(int KilledEnemies){
			this->KilledEnemies = KilledEnemies;
        }
		
		int GetWaves(){
			return this->Waves;
        }
		
		void SetWaves(int Waves){
			this->Waves = Waves;
        }
    };

UCLASS()
class RAGNAROKPROJECT_API URagnarokProjectSaveGame : public USaveGame
{
    GENERATED_BODY()

public:   
	URagnarokProjectSaveGame();
	
    UPROPERTY(VisibleAnywhere, Category = Basic)
		FString SaveSlotName;
    UPROPERTY(VisibleAnywhere, Category = Basic)
		uint32 UserIndex;
	UPROPERTY()
		TArray<FRecord> Records;
		
	UFUNCTION(BlueprintCallable)
		TArray<FRecord> GetRecords();
	FString GetSaveSlotName();
	uint32 GetUserIndex();	
	void SetSaveSlotName(FString SaveSlotName);
	void SetUserIndex(uint32 UserIndex);
	void SetRecords(TArray<FRecord> Records);

};
