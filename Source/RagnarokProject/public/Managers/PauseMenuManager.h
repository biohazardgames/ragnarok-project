#pragma once

#include "BaseManager.h"
#include "PauseMenuManager.generated.h"

UCLASS()
class RAGNAROKPROJECT_API APauseMenuManager : public ABaseManager
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> PauseMenuWidget;
	TWeakObjectPtr<class UUserWidget> pPauseMenuWidget;

	APauseMenuManager();
	UFUNCTION(BlueprintCallable)
		void SetGamePaused();
	

protected:
	virtual void BeginPlay() override;	

private:
	bool GameIsPaused;
	APlayerController* PlayerController;
};