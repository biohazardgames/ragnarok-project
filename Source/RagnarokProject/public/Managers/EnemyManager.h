#pragma once

#include "BaseManager.h"
#include "EnemyManager.generated.h"

class AEnemyFactory;
class AEnemyPortal;
class AEnemyPortalFlyweight;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnUpdateRemainingEnemiesSignature, int, EnemiesLeft);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnEarnMoneySignature, float, Money);

UCLASS()
class RAGNAROKPROJECT_API AEnemyManager : public ABaseManager
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnUpdateRemainingEnemiesSignature OnUpdateRemainingEnemies;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnEarnMoneySignature OnEarnMoney;
		
	AEnemyManager();
	UFUNCTION()
		void StartSpawningEnemies(int Wave);
	void EnemyKilled(float EarnedMoney);
	int GetKilledEnemies();

protected:
	virtual void BeginPlay() override;
	
private:
	int RemainingEnemiesToSpawn;
	int EnemiesIncrementPerWave;
	int RemainingEnemies;
	float TimeToSpawnEnemies;
	TArray<AEnemyPortal*> EnemyPortals;
	int LastSelectedPortalKey;
	int BossWaveSequence;
	bool IsBossWave;
	int KilledEnemies;
	
	void SpawnEnemy();
	void SetSpawnEnemiesTimer();
};
