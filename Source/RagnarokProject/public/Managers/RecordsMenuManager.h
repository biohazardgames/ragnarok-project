#pragma once

#include "BaseManager.h"
#include "RagnarokProjectSaveGame.h"
#include "RecordsMenuManager.generated.h"

UCLASS()
class RAGNAROKPROJECT_API ARecordsMenuManager : public ABaseManager
{
	GENERATED_BODY()

public:
	ARecordsMenuManager();	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> RecordsMenuWidget;
	TWeakObjectPtr<class UUserWidget> pRecordsMenuWidget;	
	
	UFUNCTION(BlueprintCallable)
		TArray<FRecord> GetRecords(int KilledEnemies, int Waves);
	UFUNCTION(BlueprintCallable)
		void SaveRecords(TArray<FRecord> Records);
	

protected:
	virtual void BeginPlay() override;	

private:
	TArray<FRecord> SortRecords(int GetKilledEnemies, int Waves, TArray<FRecord> Records);
};