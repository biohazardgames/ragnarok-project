// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "PurchasableObjectsManager.generated.h"

class APurchasableObject;
class APurchasableMedicalKit;
class APurchasableAmmoKit;
class AAutomaticTurret;
class ARagnarokProjectCharacter;
class AGameplayManager;

DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnObjectPurchasedSignature, float, Money);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPurchaseMedicalKitSignature, float, HealAmount);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FOnPurchaseAmmoSignature, int, PurchasedAmmo);

UCLASS()
class RAGNAROKPROJECT_API APurchasableObjectsManager : public AActor
{
	GENERATED_BODY()
	
public:	
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnObjectPurchasedSignature OnObjectPurchased;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnPurchaseMedicalKitSignature OnPurchaseMedicalKit;
	UPROPERTY(BlueprintAssignable, Category = "Events")
		FOnPurchaseAmmoSignature OnPurchaseAmmo;

	APurchasableObjectsManager();
	void ObjectPurchased(APurchasableObject* Object, float Money);
	ARagnarokProjectCharacter* GetPlayer();
	UFUNCTION()
		void StartGame();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

private:
	typedef void (APurchasableObjectsManager::*FunctionPtrType)(APurchasableObject* Object);
	TMap<FString, FunctionPtrType> PurchasedObjectsFunctions;
	ARagnarokProjectCharacter* Player;

	virtual void PurchasedAutomaticTurret(APurchasableObject* Object);
	virtual void PurchasedMedicalKit(APurchasableObject* Object);
	virtual void PurchasedAmmoKit(APurchasableObject* Object);
};
