#pragma once

#include "BaseManager.h"
#include "Runtime/Core/Public/Containers/Queue.h"
#include "LoadingScreenManager.generated.h"

UCLASS()
class RAGNAROKPROJECT_API ALoadingScreenManager : public ABaseManager
{
	GENERATED_BODY()

public:
	ALoadingScreenManager();	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> LoadingScreenWidget;
	TWeakObjectPtr<class UUserWidget> pLoadingScreenWidget;	
	TWeakObjectPtr<class UTextBlock> pTipTxtWidget;
	

protected:
	virtual void BeginPlay() override;	

private:
	UPROPERTY(EditAnywhere, Category="Stuff", meta=(MultiLine=true))
		FText TipText;
	TQueue<FString> Tips;	

	void ChangeTipMessage();
};