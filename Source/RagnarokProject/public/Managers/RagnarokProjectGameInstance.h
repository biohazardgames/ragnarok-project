
#pragma once

#include "CoreMinimal.h"
#include "Engine/GameInstance.h"
#include "RagnarokProjectGameInstance.generated.h"


UCLASS()
class RAGNAROKPROJECT_API URagnarokProjectGameInstance : public UGameInstance
{
	GENERATED_BODY()

public:
	UFUNCTION(BlueprintCallable)
		int GetKilledEnemies();		
	UFUNCTION(BlueprintCallable)
		int GetWaves();
	UFUNCTION(BlueprintCallable)
		void SetKilledEnemies(int KilledEnemies);		
	UFUNCTION(BlueprintCallable)
		void SetWaves(int Waves);

public:
	URagnarokProjectGameInstance(const FObjectInitializer& ObjectInitializer);
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
		int KilledEnemies;
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "SaveGame")
		int Waves;
};