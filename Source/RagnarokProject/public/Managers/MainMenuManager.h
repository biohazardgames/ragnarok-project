#pragma once

#include "BaseManager.h"
#include "MainMenuManager.generated.h"

UCLASS()
class RAGNAROKPROJECT_API AMainMenuManager : public ABaseManager
{
	GENERATED_BODY()

public:
	AMainMenuManager();	
	
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> MainMenuWidget;
	TWeakObjectPtr<class UUserWidget> pMainMenuWidget;	

protected:
	virtual void BeginPlay() override;	
};