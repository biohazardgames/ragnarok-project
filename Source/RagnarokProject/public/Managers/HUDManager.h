#pragma once

#include "CoreMinimal.h"
#include "TextWidgetTypes.h"
#include "GameFramework/Actor.h"
#include "HUDManager.generated.h"

class AGameplayManager;
class AEnemyManager;
class AWeapon;
class ARagnarokProjectCharacter;

UCLASS()
class RAGNAROKPROJECT_API AHUDManager : public AActor
{
	GENERATED_BODY()

public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = "Widgets")
		TSubclassOf<class UUserWidget> HUDWidget;

	AHUDManager();
	UFUNCTION()
		void StartGame();
	UFUNCTION()
		void StartWave(int Wave);
	UFUNCTION()
		void UpdateEnemiesLeft(int EnemiesLeft);
	UFUNCTION()
		void UpdateMoney(float Money);
	UFUNCTION()
		void UpdateHealthShield(float Health, float Shield, bool PlayerDamaged);
	UFUNCTION()
		void UpdateAmmo(int AmmoInClip, int CurrentAmmo);
	UFUNCTION()
		void GameOver();

protected:
	virtual void BeginPlay() override;

private:
	TWeakObjectPtr<class UUserWidget> pHUDWidget;
	TWeakObjectPtr<class UTextBlock> pWavesTxtWidget;
	TWeakObjectPtr<class UTextBlock> pEnemiesLeftTxtWidget;
	TWeakObjectPtr<class UTextBlock> pMoneyTxtWidget;
	TWeakObjectPtr<class UTextBlock> pAmmoInClipTxtWidget;
	TWeakObjectPtr<class UTextBlock> pCurrentAmmoTxtWidget;
	TWeakObjectPtr<class UImage> pBloodyScreenImageWidget;
	TWeakObjectPtr<class UImage> pSkillImageWidget;
	TWeakObjectPtr<class UProgressBar> pHealthBarWidget;
	TWeakObjectPtr<class UProgressBar> pShieldBarWidget;
	AGameplayManager* GameplayManager;
	AEnemyManager* EnemyManager;
	float BloodyScreenDuration;
	bool CanShowBloodyScreen;
	FTimerHandle Timer;
	ARagnarokProjectCharacter* Player;

	UFUNCTION()
		void HideBloodyScreen();
	UFUNCTION()
		void UpdateSkillState(bool CanUseSkill);
};