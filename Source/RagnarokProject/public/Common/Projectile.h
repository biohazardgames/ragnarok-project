#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "RagnarokProjectCharacter.h"
#include "BaseEnemyFlyweight.h"
#include "RagnarokProject.h"
#include "Projectile.generated.h"

class UProjectileMovementComponent;
class UStaticMeshComponent;
class ARagnarokProjectCharacter;

UCLASS()
class AProjectile : public AActor
{
	GENERATED_BODY()

public:
	AProjectile();
	
protected:
	UPROPERTY()
		UStaticMeshComponent* Mesh;
	UMaterialInstanceDynamic* Material;
	UPROPERTY()
		UProjectileMovementComponent* Movement;
	UPROPERTY()
		TWeakObjectPtr<UParticleSystem> ProjectileFX;
	UPROPERTY(Category = Audio, EditAnywhere)
		class USoundBase* ImpactSound;
		
	virtual void BeginPlay() override;
	UFUNCTION()
		virtual void OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit);

private:
	void PlaySound(USoundBase* Sound);
};

