// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "HealthShieldComponent.generated.h"

UCLASS()
class RAGNAROKPROJECT_API UHealthShieldComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	UHealthShieldComponent();
	float GetMaxHealth();
	float GetHealth();
	void SetMaxShield(float Shield);
	void SetShield(float Shield);
	UFUNCTION()
		virtual void Heal(float HealAmount);

protected:
	float Health;
	float MaxHealth;
	float Shield;
	float MaxShield;

	virtual void BeginPlay() override;
	virtual void HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy, AActor* DamageCauser);
};