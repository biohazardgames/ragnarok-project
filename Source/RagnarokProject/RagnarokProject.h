#pragma once

#include "CoreMinimal.h"

#define COLLISION_WEAPON					ECC_GameTraceChannel1
#define COLLISION_ENEMY						ECC_GameTraceChannel2
#define COLLISION_TRIGGER_COMPONENT			ECC_GameTraceChannel3