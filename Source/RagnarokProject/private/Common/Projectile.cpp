#include "Projectile.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"

AProjectile::AProjectile() 
{
	this->Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("ProjectileMesh"));
	
	this->Mesh->SetupAttachment(RootComponent);
	this->Mesh->bHiddenInGame = false;
	this->Mesh->OnComponentHit.AddDynamic(this, &AProjectile::OnHit);
	this->RootComponent = this->Mesh;
	
	this->Mesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->Mesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	this->Movement = CreateDefaultSubobject<UProjectileMovementComponent>(TEXT("ProjectileMovement"));
	this->Movement->SetUpdatedComponent(this->Mesh);
	this->Movement->InitialSpeed = 1000.f;
	this->Movement->MaxSpeed = 1000.f;
	this->Movement->bShouldBounce = false;
	this->Movement->ProjectileGravityScale = 0.f; 
	this->Movement->bRotationFollowsVelocity = true;

	this->InitialLifeSpan = 3.0f;
}

void AProjectile::BeginPlay()
{
	Super::BeginPlay();
}

void AProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	this->PlaySound(this->ImpactSound);
}

void AProjectile::PlaySound(USoundBase* Sound)
{
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation
		(
			this,
			Sound,
			this->GetActorLocation()
		);
	}
}