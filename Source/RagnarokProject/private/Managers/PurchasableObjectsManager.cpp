#include "PurchasableObjectsManager.h"
#include "PurchasableTurret.h"
#include "PurchasableMedicalKit.h"
#include "PurchasableAmmoKit.h"
#include "AutomaticTurret.h"
#include "RagnarokProjectCharacter.h"
#include "GameplayManager.h"

APurchasableObjectsManager::APurchasableObjectsManager()
{
	PrimaryActorTick.bCanEverTick = false;

	PurchasedObjectsFunctions.Emplace("BPPurchasableTurret_C", &APurchasableObjectsManager::PurchasedAutomaticTurret);
	PurchasedObjectsFunctions.Emplace("BPPurchasableMedicalKit_C", &APurchasableObjectsManager::PurchasedMedicalKit);
	PurchasedObjectsFunctions.Emplace("BPPurchasableAmmoKit_C", &APurchasableObjectsManager::PurchasedAmmoKit);
}

void APurchasableObjectsManager::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AGameplayManager::StaticClass(), Managers);
	Cast<AGameplayManager>(Managers[0])->OnStartGame.AddDynamic(this, &APurchasableObjectsManager::StartGame);
}

void APurchasableObjectsManager::ObjectPurchased(APurchasableObject* Object, float Money)
{
	FString PurchasedObjectClass = Object->GetClass()->GetName();
	
	if (PurchasedObjectsFunctions.Contains(PurchasedObjectClass))
	{
		(this->*(PurchasedObjectsFunctions[PurchasedObjectClass]))(Object);
	}
	
	this->OnObjectPurchased.Broadcast(Money);
}

ARagnarokProjectCharacter* APurchasableObjectsManager::GetPlayer()
{
	return this->Player;
}

void APurchasableObjectsManager::PurchasedAutomaticTurret(APurchasableObject* Object)
{
	this->GetWorld()->SpawnActor<AAutomaticTurret>(AAutomaticTurret::StaticClass(), Object->GetActorLocation(), Object->GetActorRotation());
	Object->Destroy();
}

void APurchasableObjectsManager::PurchasedMedicalKit(APurchasableObject* Object)
{
	this->OnPurchaseMedicalKit.Broadcast(Cast<APurchasableMedicalKit>(Object)->GetEffectQuantity());
}

void APurchasableObjectsManager::PurchasedAmmoKit(APurchasableObject* Object)
{
	this->OnPurchaseAmmo.Broadcast(Cast<APurchasableAmmoKit>(Object)->GetEffectQuantity());
}

void APurchasableObjectsManager::StartGame()
{
	TArray<AActor*> Players;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), ARagnarokProjectCharacter::StaticClass(), Players);
	this->Player = Cast<ARagnarokProjectCharacter>(Players[0]);
}