#include "LoadingScreenManager.h"

ALoadingScreenManager::ALoadingScreenManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void ALoadingScreenManager::BeginPlay()
{
	Super::BeginPlay();

	this->Tips.Enqueue("Remember to buy health and ammo when needed");
	this->Tips.Enqueue("Automatic turrets are expensive but they can save you in a difficult moment");
	this->Tips.Enqueue("Keep an eye on your skill meter");
	this->Tips.Enqueue("Use the environment to manage enemy waves");
	
	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->LoadingScreenWidget)
	{
		this->pLoadingScreenWidget = CreateWidget<UUserWidget>(this->GetGameInstance(), this->LoadingScreenWidget);

		if (this->pLoadingScreenWidget.IsValid())
		{
			this->pLoadingScreenWidget->AddToViewport();
			this->pTipTxtWidget = (UTextBlock*)this->pLoadingScreenWidget->GetWidgetFromName("TipText");
			
			this->ChangeTipMessage();
			
			this->GetWorldTimerManager().SetTimer
			(
				this->Timer,
				this,
				&ALoadingScreenManager::ChangeTipMessage,
				5.f,
				true
			);
		}				
	}
}

void ALoadingScreenManager::ChangeTipMessage()
{
	FString Tip;
	this->Tips.Dequeue(Tip);
	this->pTipTxtWidget->SetText(FText::FromString(Tip));
	this->Tips.Enqueue(Tip);
}