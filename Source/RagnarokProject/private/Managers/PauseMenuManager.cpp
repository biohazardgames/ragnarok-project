#include "PauseMenuManager.h"

APauseMenuManager::APauseMenuManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
	this->GameIsPaused = false;
}

void APauseMenuManager::BeginPlay()
{
	Super::BeginPlay();

	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->PauseMenuWidget)
	{
		this->pPauseMenuWidget = CreateWidget<UUserWidget>(this->GetGameInstance(), this->PauseMenuWidget);			
	}

	this->PlayerController = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
}

void APauseMenuManager::SetGamePaused()
{
	if (this->GameIsPaused)
	{
		this->pPauseMenuWidget->RemoveFromParent();
		this->GameIsPaused = false;
		if (this->PlayerController)
		{
			this->PlayerController->bShowMouseCursor = false;
			this->PlayerController->SetInputMode(FInputModeGameOnly());
		}
	}
	else
	{
		this->pPauseMenuWidget->AddToViewport();
		this->GameIsPaused = true;		
	}	

	if (this->PlayerController)
	{
		this->PlayerController->SetPause(GameIsPaused);
	}
}