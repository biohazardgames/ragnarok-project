#include "GameplayManager.h"
#include "EnemyManager.h"
#include "PurchasableObjectsManager.h"
#include "HUDManager.h"
#include "RagnarokProjectCharacter.h"
#include "RagnarokProjectGameInstance.h"

AGameplayManager::AGameplayManager() : Super()
{
	this->Wave = 1;
	this->TimeToStartWave = 10.f;
}

void AGameplayManager::BeginPlay()
{
	Super::BeginPlay();
	
	this->InitializeGame();
}

void AGameplayManager::InitializeGame()
{
	TArray<AActor*> EnemyManagers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyManager::StaticClass(), EnemyManagers);
	Cast<AEnemyManager>(EnemyManagers[0])->OnUpdateRemainingEnemies.AddDynamic(this, &AGameplayManager::EnemyKilled);

	this->GetWorldTimerManager().SetTimer
	(
		this->Timer,
		this,
		&AGameplayManager::StartGame,
		this->TimeToStartWave,
		false
	);
}

void AGameplayManager::StartGame()
{
	TArray<AActor*> Players;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), ARagnarokProjectCharacter::StaticClass(), Players);
	Cast<ARagnarokProjectCharacter>(Players[0])->OnDiePlayer.AddDynamic(this, &AGameplayManager::GameOver);

	this->OnStartGame.Broadcast();
	this->StartWave();
}

void AGameplayManager::StartWave()
{
	this->OnStartWave.Broadcast(this->Wave);
}

void AGameplayManager::SetWaveTimer()
{
	this->GetWorldTimerManager().ClearTimer(this->Timer);
	this->GetWorldTimerManager().SetTimer
	(
		this->Timer,
		this,
		&AGameplayManager::StartWave,
		this->TimeToStartWave,
		false
	);
}

void AGameplayManager::EnemyKilled(int RemainingEnemies)
{
	if (RemainingEnemies == 0)
	{
		this->Wave += 1;
		this->SetWaveTimer();
	}
}

void AGameplayManager::GameOver()
{
	this->GetWorldTimerManager().ClearTimer(this->Timer);

	URagnarokProjectGameInstance* RagnarokProjectGameInstance = Cast<URagnarokProjectGameInstance>(this->GetGameInstance());
	if (RagnarokProjectGameInstance)
	{
		RagnarokProjectGameInstance->SetWaves(this->Wave);

		TArray<AActor*> EnemyManagers;
		UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyManager::StaticClass(), EnemyManagers);
		RagnarokProjectGameInstance->SetKilledEnemies(Cast<AEnemyManager>(EnemyManagers[0])->GetKilledEnemies());
		
	}

	this->SwapLevel("RecordsMenu");
}

