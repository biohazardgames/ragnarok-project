#include "MainMenuManager.h"

AMainMenuManager::AMainMenuManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void AMainMenuManager::BeginPlay()
{
	Super::BeginPlay();

	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->MainMenuWidget)
	{
		this->pMainMenuWidget = CreateWidget<UUserWidget>(this->GetGameInstance(), this->MainMenuWidget);

		if (this->pMainMenuWidget.IsValid())
		{
			this->pMainMenuWidget->AddToViewport();
		}				
	}
	
	this->PlayManagerSound(this->AmbientalSong);
}