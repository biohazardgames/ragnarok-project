
#include "RagnarokProjectSaveGame.h"
#include "RagnarokProject.h"

URagnarokProjectSaveGame::URagnarokProjectSaveGame()
{
	this->SaveSlotName = TEXT("SaveData");
	this->UserIndex = 0;
}

FString URagnarokProjectSaveGame::GetSaveSlotName()
{
	return this->SaveSlotName;
}

uint32 URagnarokProjectSaveGame::GetUserIndex()
{
	return this->UserIndex;
}

TArray<FRecord> URagnarokProjectSaveGame::GetRecords()
{
	return this->Records;
}

void URagnarokProjectSaveGame::SetSaveSlotName(FString SaveSlotName)
{
	this->SaveSlotName = SaveSlotName;
}

void URagnarokProjectSaveGame::SetUserIndex(uint32 UserIndex)
{
	this->UserIndex = UserIndex;
}

void URagnarokProjectSaveGame::SetRecords(TArray<FRecord> Records)
{
	this->Records = Records;
}