#include "ControlsMenuManager.h"

AControlsMenuManager::AControlsMenuManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void AControlsMenuManager::BeginPlay()
{
	Super::BeginPlay();

	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->ControlsMenuWidget)
	{
		this->pControlsMenuWidget = CreateWidget<UUserWidget>(this->GetGameInstance(), this->ControlsMenuWidget);

		if (this->pControlsMenuWidget.IsValid())
		{
			this->pControlsMenuWidget->AddToViewport();
		}				
	}
	
	this->PlayManagerSound(this->AmbientalSong);
}