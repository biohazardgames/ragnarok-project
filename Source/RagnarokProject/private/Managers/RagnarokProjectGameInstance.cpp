#include "RagnarokProjectGameInstance.h"

URagnarokProjectGameInstance::URagnarokProjectGameInstance(const FObjectInitializer& ObjectInitializer) : Super(ObjectInitializer)
{
	this->KilledEnemies = -1;
	this->Waves = -1;
}

int URagnarokProjectGameInstance::GetKilledEnemies()
{
	return this->KilledEnemies;
}

int URagnarokProjectGameInstance::GetWaves()
{
	return this->Waves;
}

void URagnarokProjectGameInstance::SetKilledEnemies(int KilledEnemies)
{
	this->KilledEnemies = KilledEnemies;
}

void URagnarokProjectGameInstance::SetWaves(int Waves)
{
	this->Waves = Waves;
}