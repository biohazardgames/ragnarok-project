#include "CreditsMenuManager.h"

ACreditsMenuManager::ACreditsMenuManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void ACreditsMenuManager::BeginPlay()
{
	Super::BeginPlay();

	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->CreditsMenuWidget)
	{
		this->pCreditsMenuWidget = CreateWidget<UUserWidget>(this->GetGameInstance(), this->CreditsMenuWidget);

		if (this->pCreditsMenuWidget.IsValid())
		{
			this->pCreditsMenuWidget->AddToViewport();
		}				
	}
	
	this->PlayManagerSound(this->AmbientalSong);
}