#include "EnemyManager.h"
#include "EnemyPortal.h"
#include "EnemyFactory.h"
#include "GameplayManager.h"
#include "EnemyPortalFlyweight.h"

AEnemyManager::AEnemyManager() : Super()
{
	this->EnemiesIncrementPerWave = 3;
	this->RemainingEnemies = this->EnemiesIncrementPerWave;
	this->RemainingEnemiesToSpawn = this->RemainingEnemies;
	this->TimeToSpawnEnemies = 5.f;
	this->LastSelectedPortalKey = NULL;
	this->BossWaveSequence = 5;
	this->IsBossWave = false;
	this->KilledEnemies = 0;
}

void AEnemyManager::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AGameplayManager::StaticClass(), Managers);
	this->GameplayManager = Cast<AGameplayManager>(Managers[0]);
	this->GameplayManager->OnStartWave.AddDynamic(this, &AEnemyManager::StartSpawningEnemies);

	FVector Location = FVector(1.f, 1.f, 1.f);
	Cast<AEnemyPortalFlyweight>(this->GetWorld()->SpawnActor(AEnemyPortalFlyweight::StaticClass(), &Location));
	Cast<AEnemyFactory>(this->GetWorld()->SpawnActor(AEnemyFactory::StaticClass(), &Location));

	TArray<AActor*> Portals;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyPortal::StaticClass(), Portals);

	for (int Index = 0; Index < Portals.Num(); Index++)
	{
		if (Portals.IsValidIndex(Index))
		{
			AEnemyPortal* EnemyPortal = Cast<AEnemyPortal>(Portals[Index]);
			this->EnemyPortals.Emplace(EnemyPortal);
			EnemyPortal->StartGame();
		}
	}
}

void AEnemyManager::SetSpawnEnemiesTimer()
{
	this->GetWorldTimerManager().ClearTimer(this->Timer);
	this->GetWorldTimerManager().SetTimer
	(
		this->Timer,
		this,
		&AEnemyManager::SpawnEnemy,
		this->TimeToSpawnEnemies,
		true
	);
}

void AEnemyManager::StartSpawningEnemies(int Wave)
{
	if ((Wave % this->BossWaveSequence) == 0)
	{
		this->RemainingEnemies = 1;
		this->IsBossWave = true;
	}
	else
	{
		this->RemainingEnemies = this->EnemiesIncrementPerWave * Wave;
		this->IsBossWave = false;
	}

	this->RemainingEnemiesToSpawn = this->RemainingEnemies;
	this->OnUpdateRemainingEnemies.Broadcast(this->RemainingEnemies);
	this->SetSpawnEnemiesTimer();
}

void AEnemyManager::SpawnEnemy()
{	
	int SelectedPortal = FMath::RandRange(0, this->EnemyPortals.Num() - 1);
	this->LastSelectedPortalKey = (SelectedPortal != this->LastSelectedPortalKey)
		? SelectedPortal
		: FMath::RandRange(0, this->EnemyPortals.Num() - 1);


	if (this->EnemyPortals.Num() > 0 && this->EnemyPortals.IsValidIndex(this->LastSelectedPortalKey))
	{
		this->EnemyPortals[this->LastSelectedPortalKey]->SpawnEnemy(this->IsBossWave);
		this->RemainingEnemiesToSpawn -= 1;

		if (this->RemainingEnemiesToSpawn == 0)
		{
			this->GetWorldTimerManager().ClearTimer(this->Timer);
		}
	}
}

void AEnemyManager::EnemyKilled(float EarnedMoney)
{
	this->KilledEnemies += 1;
	this->OnUpdateRemainingEnemies.Broadcast(this->RemainingEnemies -= 1);
	this->OnEarnMoney.Broadcast(EarnedMoney);
}

int AEnemyManager::GetKilledEnemies()
{
	return this->KilledEnemies;
}