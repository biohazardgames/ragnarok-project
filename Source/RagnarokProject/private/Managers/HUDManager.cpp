#include "HUDManager.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "UMG.h"
#include "Blueprint/UserWidget.h"
#include "GameplayManager.h"
#include "EnemyManager.h"
#include "RagnarokProjectCharacter.h"
#include "Weapon.h"

AHUDManager::AHUDManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void AHUDManager::BeginPlay()
{
	Super::BeginPlay();

	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->HUDWidget)
	{
		this->pHUDWidget = CreateWidget<UUserWidget>(GetGameInstance(), this->HUDWidget);

		if (this->pHUDWidget.IsValid())
		{
			this->pHUDWidget->AddToViewport();
			this->pWavesTxtWidget = (UTextBlock*)this->pHUDWidget->GetWidgetFromName("WavesText");
			this->pEnemiesLeftTxtWidget = (UTextBlock*)this->pHUDWidget->GetWidgetFromName("EnemiesLeftText");
			this->pMoneyTxtWidget = (UTextBlock*)this->pHUDWidget->GetWidgetFromName("MoneyText");
			this->pAmmoInClipTxtWidget = (UTextBlock*)this->pHUDWidget->GetWidgetFromName("AmmoInClipText");
			this->pCurrentAmmoTxtWidget = (UTextBlock*)this->pHUDWidget->GetWidgetFromName("CurrentAmmoText");
			this->pBloodyScreenImageWidget = (UImage*)this->pHUDWidget->GetWidgetFromName("BloodyScreenImage");
			this->pSkillImageWidget = (UImage*)this->pHUDWidget->GetWidgetFromName("SkillImage");
			this->pHealthBarWidget = (UProgressBar*)this->pHUDWidget->GetWidgetFromName("HealthBar");
			this->pShieldBarWidget = (UProgressBar*)this->pHUDWidget->GetWidgetFromName("ShieldBar");

			this->pBloodyScreenImageWidget->SetVisibility(ESlateVisibility::Hidden);
			this->pSkillImageWidget->SetVisibility(ESlateVisibility::Visible);
		}
	}

	this->BloodyScreenDuration = 1.f;
	this->CanShowBloodyScreen = true;

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AGameplayManager::StaticClass(), Managers);
	this->GameplayManager = Cast<AGameplayManager>(Managers[0]);
	this->GameplayManager->OnStartGame.AddDynamic(this, &AHUDManager::StartGame);
	this->GameplayManager->OnStartWave.AddDynamic(this, &AHUDManager::StartWave);
}

void AHUDManager::UpdateEnemiesLeft(int EnemiesLeft)
{
	this->pEnemiesLeftTxtWidget->SetText(FText::AsNumber(EnemiesLeft));
}

void AHUDManager::UpdateMoney(float Money)
{
	this->pMoneyTxtWidget->SetText(FText::AsNumber(Money));
}

void AHUDManager::UpdateHealthShield(float Health, float Shield, bool PlayerDamaged)
{
	this->pHealthBarWidget->SetPercent(Health);
	this->pShieldBarWidget->SetPercent(Shield);

	if (this->CanShowBloodyScreen && PlayerDamaged)
	{
		this->pBloodyScreenImageWidget->SetVisibility(ESlateVisibility::Visible);
		this->CanShowBloodyScreen = false;

		this->GetWorldTimerManager().ClearTimer(this->Timer);
		this->GetWorldTimerManager().SetTimer
		(
			this->Timer,
			this,
			&AHUDManager::HideBloodyScreen,
			this->BloodyScreenDuration,
			false
		);
	}
}

void AHUDManager::UpdateAmmo(int AmmoInClip, int CurrentAmmo)
{
	this->pAmmoInClipTxtWidget->SetText(FText::AsNumber(AmmoInClip));
	this->pCurrentAmmoTxtWidget->SetText(FText::AsNumber(CurrentAmmo));
}

void AHUDManager::StartGame()
{
	TArray<AActor*> Players;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), ARagnarokProjectCharacter::StaticClass(), Players);
	this->Player = Cast<ARagnarokProjectCharacter>(Players[0]);
	this->Player->OnUpdateMoney.AddDynamic(this, &AHUDManager::UpdateMoney);
	this->Player->GetHealthShieldComponent()->OnUpdateHealthShield.AddDynamic(this, &AHUDManager::UpdateHealthShield);
	this->Player->OnUpdateAmmo.AddDynamic(this, &AHUDManager::UpdateAmmo);
	this->Player->OnUpdateSkillState.AddDynamic(this, &AHUDManager::UpdateSkillState);
	this->Player->OnDiePlayer.AddDynamic(this, &AHUDManager::GameOver);
	
	TArray<AActor*> EnemyManagers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyManager::StaticClass(), EnemyManagers);
	Cast<AEnemyManager>(EnemyManagers[0])->OnUpdateRemainingEnemies.AddDynamic(this, &AHUDManager::UpdateEnemiesLeft);
}

void AHUDManager::StartWave(int Wave)
{
	this->pWavesTxtWidget->SetText(FText::AsNumber(Wave));
}

void AHUDManager::HideBloodyScreen()
{
	this->CanShowBloodyScreen = true;
	this->pBloodyScreenImageWidget->SetVisibility(ESlateVisibility::Hidden);
}

void AHUDManager::UpdateSkillState(bool CanUseSkill)
{
	if (CanUseSkill)
	{
		this->pSkillImageWidget->SetVisibility(ESlateVisibility::Visible);
	}
	else
	{
		this->pSkillImageWidget->SetVisibility(ESlateVisibility::Hidden);
	}
}

void AHUDManager::GameOver()
{
	
}