#include "RecordsMenuManager.h"

ARecordsMenuManager::ARecordsMenuManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void ARecordsMenuManager::BeginPlay()
{
	Super::BeginPlay();

	/*Gets the reference to the widget and adds it to the viewport*/
	if (this->RecordsMenuWidget)
	{
		this->pRecordsMenuWidget = CreateWidget<UUserWidget>(this->GetGameInstance(), this->RecordsMenuWidget);

		if (this->pRecordsMenuWidget.IsValid())
		{
			this->pRecordsMenuWidget->AddToViewport();
		}				
	}
	
	this->PlayManagerSound(this->AmbientalSong);
}

/*It loads all records from the save data and sorts them if the save data contains one or more registries*/
TArray<FRecord> ARecordsMenuManager::GetRecords(int KilledEnemies, int Waves)
{
	URagnarokProjectSaveGame* LoadGameInstance = Cast<URagnarokProjectSaveGame>(UGameplayStatics::CreateSaveGameObject(URagnarokProjectSaveGame::StaticClass()));
	LoadGameInstance = Cast<URagnarokProjectSaveGame>(UGameplayStatics::LoadGameFromSlot(LoadGameInstance->GetSaveSlotName(), LoadGameInstance->GetUserIndex()));
	TArray<FRecord> Records;
	TArray<FRecord> SortedRecords;
	FRecord Record;

	if (KilledEnemies != -1 && Waves != -1)
	{		
		Record.SetKilledEnemies(KilledEnemies);
		Record.SetWaves(Waves);

		if (LoadGameInstance)
		{
			Records = LoadGameInstance->GetRecords();
			Records.Emplace(Record);
			SortedRecords = SortRecords(KilledEnemies, Waves, Records);
		}
		else
		{
			SortedRecords.Emplace(Record);
		}
	}
	else
	{
		if (LoadGameInstance)
		{
			SortedRecords = SortRecords(KilledEnemies, Waves, LoadGameInstance->GetRecords());
		}
	}
	
	return SortedRecords;
}

/*Used to sort the array of Records using the Bubble Sort algoritm*/
TArray<FRecord> ARecordsMenuManager::SortRecords(int KilledEnemies, int Waves, TArray<FRecord> Records)
{
	int N = Records.Num(),
		NewN = 0;		

	while (N != 0)
	{
		NewN = 0;
		for (int i = 1; i <= N - 1; i++)
		{
			if (Records[i - 1].GetKilledEnemies() < Records[i].GetKilledEnemies())
			{
				Records.Swap(i - 1, i);
				NewN = i;
			}
		}
		N = NewN;
	}

	return Records;
}

/*It stores all records into the save data*/
void ARecordsMenuManager::SaveRecords(TArray<FRecord> Records)
{
	URagnarokProjectSaveGame* SaveGameInstance = Cast<URagnarokProjectSaveGame>(UGameplayStatics::CreateSaveGameObject(URagnarokProjectSaveGame::StaticClass()));
	SaveGameInstance->SetRecords(Records);
	UGameplayStatics::SaveGameToSlot(SaveGameInstance, SaveGameInstance->GetSaveSlotName(), SaveGameInstance->GetUserIndex());
}