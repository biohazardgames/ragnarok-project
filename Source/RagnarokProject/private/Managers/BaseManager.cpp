#include "BaseManager.h"

ABaseManager::ABaseManager()
{
	this->PrimaryActorTick.bCanEverTick = false;
	
	auto SoundAsset = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Menus/AmbientalSong.AmbientalSong'"));
	if (SoundAsset.Succeeded())
	{
		this->AmbientalSong = SoundAsset.Object;
	}
	
	SoundAsset = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Menus/ButtonHoveredSound.ButtonHoveredSound'"));
	if (SoundAsset.Succeeded())
	{
		this->ButtonHoveredSound = SoundAsset.Object;
	}
}

void ABaseManager::BeginPlay()
{
	Super::BeginPlay();	
}

void ABaseManager::PlayManagerSound(USoundBase* SoundToPlay)
{
	if (SoundToPlay)
	{
		UGameplayStatics::PlaySound2D(GetWorld(), SoundToPlay);
	}
}

void ABaseManager::OnHoveredButton()
{
	this->PlayManagerSound(ButtonHoveredSound);
}

/*Used to change to another level of the game*/
void ABaseManager::SwapLevel(FString levelName)
{
	UGameplayStatics::OpenLevel(GetWorld(), FName(*levelName));
}

/*Used to quit the game when the Exit button is clicked on*/
void ABaseManager::QuitGameButtonClicked()
{
	FGenericPlatformMisc::RequestExit(false);
}
