#include "EnemyHealthShieldComponent.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "BaseEnemy.h"

UEnemyHealthShieldComponent::UEnemyHealthShieldComponent() : Super()
{
	this->MaxShield = 0.f;
}

// Called when the game starts
void UEnemyHealthShieldComponent::BeginPlay()
{
	Super::BeginPlay();

	Cast<ABaseEnemy>(this->GetOwner())->OnChangeHealthShield(this->MaxHealth / 100.f, this->MaxShield / 100.f);
}

void UEnemyHealthShieldComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy,
	AActor* DamageCauser)
{
	Super::HandleTakeAnyDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);

	if (DamageCauser != DamagedActor && DamageCauser != this->GetOwner())
	{
		Cast<ABaseEnemy>(this->GetOwner())->OnChangeHealthShield(this->Health / 100.f, this->Shield / 100.f);

		if (this->Health == 0.f && this->GetOwner())
		{
			Cast<ABaseEnemy>(this->GetOwner())->Death(DamageCauser);
		}
	}
}