#include "EnemyFactory.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "BaseEnemyFlyweight.h"
#include "StandardEnemyFlyweight.h"
#include "SorcererEnemyFlyweight.h"

AEnemyFactory::AEnemyFactory()
{
	this->PrimaryActorTick.bCanEverTick = false;
}

void AEnemyFactory::BeginPlay()
{
	Super::BeginPlay();

	this->EnemyClasses.Emplace("StandardEnemy", AStandardEnemyFlyweight::StaticClass());
	this->EnemyClasses.Emplace("SorcererEnemy", ASorcererEnemyFlyweight::StaticClass());
}

ABaseEnemyFlyweight* AEnemyFactory::GetCommonState(FString CommonStateName)
{
	if (!this->EnemyCommonStates.Contains(CommonStateName))
	{
		FVector Location = FVector(1.f, 1.f, 1.f);
		this->EnemyCommonStates.Emplace(CommonStateName, Cast<ABaseEnemyFlyweight>(this->GetWorld()->SpawnActor(this->EnemyClasses[CommonStateName], &Location)));
	} 
	return this->EnemyCommonStates[CommonStateName];
}

