#include "AIControllerSorcererEnemy.h"
#include "SorcererEnemy.h"

AAIControllerSorcererEnemy::AAIControllerSorcererEnemy() : Super()
{
	
}

void AAIControllerSorcererEnemy::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	ASorcererEnemy* Char = Cast<ASorcererEnemy>(InPawn);

	if (Char && Char->GetBehavior())
	{
		this->BlackboardComp->InitializeBlackboard(*Char->GetBehavior()->BlackboardAsset);
		this->PlayerKeyId = this->BlackboardComp->GetKeyID("Player");
		this->BehaviorTreeComp->StartTree(*Char->GetBehavior());
	}
}