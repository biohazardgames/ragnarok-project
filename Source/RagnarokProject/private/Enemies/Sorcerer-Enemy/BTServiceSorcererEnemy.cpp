#include "BTServiceSorcererEnemy.h"
#include "AIControllerSorcererEnemy.h"
#include "SorcererEnemy.h"

UBTServiceSorcererEnemy::UBTServiceSorcererEnemy() : Super()
{
	
}

void UBTServiceSorcererEnemy::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AAIControllerSorcererEnemy* EnemyPC = Cast<AAIControllerSorcererEnemy>(OwnerComp.GetAIOwner());
	if (EnemyPC)
	{
		ARagnarokProjectCharacter* Player = Cast<ARagnarokProjectCharacter>(this->GetWorld()->GetFirstPlayerController()->GetPawn());
		if (Player)
		{
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->PlayerKeyId, Player);
		}
	}
}