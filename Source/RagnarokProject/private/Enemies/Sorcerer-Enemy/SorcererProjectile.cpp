#include "SorcererProjectile.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "SorcererEnemy.h"

ASorcererProjectile::ASorcererProjectile() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(0.3f, 0.3f, 0.3f));
	}

	auto ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Enemies/SorcererEnemy/Projectile/ElectricShot.ElectricShot'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->ProjectileFX = ParticleSystemAsset.Object;
	}

	auto MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/RagnarokProject/Enemies/SorcererEnemy/Projectile/ProjectileMaterial.ProjectileMaterial'"));
	if (MaterialAsset.Succeeded())
	{
		this->Material = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->Mesh);
		this->Mesh->SetMaterial(0, Material);
	}

	auto Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Enemies/SorcererEnemy/Sounds/ProjectileImpact.ProjectileImpact'"));

	if (Audio.Succeeded())
	{
		this->ImpactSound = Audio.Object;
	}
}

void ASorcererProjectile::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::SpawnEmitterAttached(this->ProjectileFX.Get(), this->Mesh);
}

void ASorcererProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);

	if (OtherActor && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		FPointDamageEvent DmgEvent;
		DmgEvent.Damage = Cast<ASorcererEnemy>(this->GetOwner())->GetCommonState()->GetDamage();
		Cast<ARagnarokProjectCharacter>(OtherActor)->TakeDamage(DmgEvent.Damage, DmgEvent, Cast<ASorcererEnemy>(this->GetOwner())->GetController(), this);
	}

	this->Destroy();
}