#include "SorcererEnemyFlyweight.h"

ASorcererEnemyFlyweight::ASorcererEnemyFlyweight() : Super()
{
	this->PrimaryActorTick.bCanEverTick = false;
	
	this->Damage = 40.f;
	this->EarnedMoney = 300.f;
	this->AttackCooldown = 1.f;
	this->Speed = 200.f;

	this->AttackSound = NULL;

	auto Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Enemies/SorcererEnemy/Sounds/LaunchProjectile.LaunchProjectile'"));

	if (Audio.Succeeded())
	{
		this->LaunchProjectileSound = Audio.Object;
	}

	auto AnimationBlueprint = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Enemies/SorcererEnemy/Animations/AnimBP.AnimBP_C'"));
	if (AnimationBlueprint.Succeeded())
	{
		this->AnimationBlueprint = AnimationBlueprint.Object;
	}

	auto AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/SorcererEnemy/Animations/AnimMontageAttack.AnimMontageAttack'"));

	if (AnimationMontage.Succeeded())
	{
		this->AttackAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimMontageNeckBite.AnimMontageNeckBite'"));

	if (AnimationMontage.Succeeded())
	{
		this->AttackAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/SorcererEnemy/Animations/AnimMontageDeath1.AnimMontageDeath1'"));

	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/SorcererEnemy/Animations/AnimMontageDeath2.AnimMontageDeath2'"));

	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/SorcererEnemy/Animations/AnimMontageDeath3.AnimMontageDeath3'"));

	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimations.Emplace(AnimationMontage.Object);
	}
}

void ASorcererEnemyFlyweight::BeginPlay()
{
	Super::BeginPlay();
}