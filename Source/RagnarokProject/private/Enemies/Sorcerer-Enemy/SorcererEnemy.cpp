#include "SorcererEnemy.h"
#include "EngineMinimal.h"
#include "EnemyFactory.h"
#include "SorcererEnemyFlyweight.h"
#include "SorcererProjectile.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/AssetRegistry/Public/AssetData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "AIControllerSorcererEnemy.h"

ASorcererEnemy::ASorcererEnemy() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/RagnarokProject/Enemies/SorcererEnemy/Model/SorcererEnemy.SorcererEnemy'"));

	if (MeshAsset.Succeeded())
	{
		this->GetMesh()->SetSkeletalMesh(MeshAsset.Object);
		this->GetMesh()->SetWorldTransform(FTransform(FVector(-20.f, 0.f, -80.f)));

		auto AnimationBlueprint = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Enemies/SorcererEnemy/Animations/AnimBP.AnimBP_C'"));
		if (AnimationBlueprint.Succeeded())
		{
			this->GetMesh()->SetAnimInstanceClass(AnimationBlueprint.Object);
		}
	}
	
	auto PhysicsAsset = ConstructorHelpers::FObjectFinder<UPhysicsAsset>(TEXT("PhysicsAsset'/Game/RagnarokProject/Enemies/SorcererEnemy/Model/SorcererEnemy_PhysicsAsset.SorcererEnemy_PhysicsAsset'"));
	if (PhysicsAsset.Succeeded())
	{
		this->GetMesh()->SetPhysicsAsset(PhysicsAsset.Object);
		this->GetMesh()->SetSimulatePhysics(false);
	}

	this->ProjectileSocketName = "ProjectileSocket";
	
	FName BTPath = TEXT("BehaviorTree'/Game/RagnarokProject/Enemies/SorcererEnemy/AI/BTSorcererEnemy.BTSorcererEnemy'"); 
	this->Behavior = Cast<UBehaviorTree>(StaticLoadObject(UBehaviorTree::StaticClass(), NULL, *BTPath.ToString()));

	this->AIControllerClass = AAIControllerSorcererEnemy::StaticClass();
	this->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void ASorcererEnemy::BeginPlay()
{
	Super::BeginPlay();	

	this->CommonState = Cast<ASorcererEnemyFlyweight>(this->Factory->GetCommonState("SorcererEnemy"));

	this->GetCharacterMovement()->MaxWalkSpeed = this->CommonState->GetSpeed();
	
	this->HealthShield->SetMaxShield(100.f);
	this->HealthShield->SetShield(100.f);
}

void ASorcererEnemy::LaunchProjectile()
{
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	ASorcererProjectile* Projectile = GetWorld()->SpawnActor<ASorcererProjectile>
			(
				ASorcererProjectile::StaticClass(),
				this->GetMesh()->GetSocketLocation(FName(TEXT("ProjectileSocket"))),
				this->GetActorRotation()
			);

	Projectile->SetOwner(this);
	this->PlaySound(this->CommonState->GetLaunchProjectileSound());
}