#include "BTTaskAttackSorcererEnemy.h"
#include "SorcererEnemy.h"
#include "AIControllerSorcererEnemy.h"

EBTNodeResult::Type UBTTaskAttackSorcererEnemy::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIControllerSorcererEnemy* CharPC = Cast<AAIControllerSorcererEnemy>(OwnerComp.GetAIOwner());

	if (CharPC)
	{
		ASorcererEnemy* SorcererEnemy = Cast<ASorcererEnemy>(CharPC->GetPawn());
		ARagnarokProjectCharacter* Player = Cast<ARagnarokProjectCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->PlayerKeyId));

		if (SorcererEnemy && Player && SorcererEnemy->GetCanAttack() && SorcererEnemy->GetDistanceTo(Player) >= (SorcererEnemy->GetCommonState()->GetMinDistanceToPlayer("SorcererEnemy")))
		{
			SorcererEnemy->Attack(Cast<AActor>(Player));
			return EBTNodeResult::Succeeded;
		}
	}
	return EBTNodeResult::Failed;
}