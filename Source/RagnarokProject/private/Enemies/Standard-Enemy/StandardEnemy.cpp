#include "StandardEnemy.h"
#include "EngineMinimal.h"
#include "EnemyFactory.h"
#include "StandardEnemyFlyweight.h"
#include "Runtime/CoreUObject/Public/UObject/UObjectGlobals.h"
#include "Runtime/AssetRegistry/Public/AssetData.h"
#include "Runtime/AIModule/Classes/BehaviorTree/BehaviorTree.h"
#include "AIControllerStandardEnemy.h"

AStandardEnemy::AStandardEnemy() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/RagnarokProject/Enemies/StandardEnemy/Model/StandardEnemy.StandardEnemy'"));

	if (MeshAsset.Succeeded())
	{
		this->GetMesh()->SetSkeletalMesh(MeshAsset.Object);
		this->GetMesh()->SetWorldTransform(FTransform(FVector(-20.f, 0.f, -80.f)));

		auto AnimationBlueprint = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimBP.AnimBP_C'"));
		if (AnimationBlueprint.Succeeded())
		{
			this->GetMesh()->SetAnimInstanceClass(AnimationBlueprint.Object);
		}
	}
	
	auto PhysicsAsset = ConstructorHelpers::FObjectFinder<UPhysicsAsset>(TEXT("PhysicsAsset'/Game/RagnarokProject/Enemies/StandardEnemy/Model/StandardEnemy_PhysicsAsset.StandardEnemy_PhysicsAsset'"));
	if (PhysicsAsset.Succeeded())
	{
		this->GetMesh()->SetPhysicsAsset(PhysicsAsset.Object);
		this->GetMesh()->SetSimulatePhysics(false);
	}

	FName BTPath = TEXT("BehaviorTree'/Game/RagnarokProject/Enemies/StandardEnemy/AI/BTStandardEnemy.BTStandardEnemy'"); 
	this->Behavior = Cast<UBehaviorTree>(StaticLoadObject(UBehaviorTree::StaticClass(), NULL, *BTPath.ToString()));

	this->AIControllerClass = AAIControllerStandardEnemy::StaticClass();
	this->AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
}

void AStandardEnemy::BeginPlay()
{
	Super::BeginPlay();	

	this->CommonState = Cast<AStandardEnemyFlyweight>(this->Factory->GetCommonState("StandardEnemy"));

	this->GetCharacterMovement()->MaxWalkSpeed = this->CommonState->GetSpeed();
}

void AStandardEnemy::Attack(AActor* Target)
{
	Super::Attack(Target);

	FPointDamageEvent DmgEvent;
	DmgEvent.Damage = this->CommonState->GetDamage();
	Target->TakeDamage(DmgEvent.Damage, DmgEvent, this->GetController(), this);
}