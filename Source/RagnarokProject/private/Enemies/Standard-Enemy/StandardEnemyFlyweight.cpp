#include "StandardEnemyFlyweight.h"

AStandardEnemyFlyweight::AStandardEnemyFlyweight() : Super()
{
	this->PrimaryActorTick.bCanEverTick = false;
	
	this->Damage = 40.f;
	this->EarnedMoney = 50.f;
	this->AttackCooldown = 0.5f;
	this->Speed = 300.f;

	auto Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Enemies/StandardEnemy/Sounds/Attack.Attack'"));

	if (Audio.Succeeded())
	{
		this->AttackSound = Audio.Object;
	}

	auto AnimationBlueprint = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimBP.AnimBP_C'"));
	if (AnimationBlueprint.Succeeded())
	{
		this->AnimationBlueprint = AnimationBlueprint.Object;
	}

	auto AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimMontageAttackOneHand.AnimMontageAttackOneHand'"));

	if (AnimationMontage.Succeeded())
	{
		this->AttackAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimMontageNeckBite.AnimMontageNeckBite'"));

	if (AnimationMontage.Succeeded())
	{
		this->AttackAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimMontageDeath1.AnimMontageDeath1'"));

	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimMontageDeath2.AnimMontageDeath2'"));

	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimations.Emplace(AnimationMontage.Object);
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Enemies/StandardEnemy/Animations/AnimMontageDeath3.AnimMontageDeath3'"));

	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimations.Emplace(AnimationMontage.Object);
	}
}

void AStandardEnemyFlyweight::BeginPlay()
{
	Super::BeginPlay();
}