#include "BTTaskAttackStandardEnemy.h"
#include "StandardEnemy.h"
#include "AIControllerStandardEnemy.h"

EBTNodeResult::Type UBTTaskAttackStandardEnemy::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIControllerStandardEnemy* CharPC = Cast<AAIControllerStandardEnemy>(OwnerComp.GetAIOwner());

	if (CharPC)
	{
		AStandardEnemy* StandardEnemy = Cast<AStandardEnemy>(CharPC->GetPawn());
		ARagnarokProjectCharacter* Player = Cast<ARagnarokProjectCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->PlayerKeyId));

		if (StandardEnemy && Player && StandardEnemy->GetCanAttack() && StandardEnemy->GetIsNearPlayer())
		{
			StandardEnemy->Attack(Cast<AActor>(Player));
			return EBTNodeResult::Succeeded;
		}
	}
	return EBTNodeResult::Failed;
}