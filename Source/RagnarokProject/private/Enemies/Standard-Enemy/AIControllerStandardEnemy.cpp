#include "AIControllerStandardEnemy.h"
#include "StandardEnemy.h"

AAIControllerStandardEnemy::AAIControllerStandardEnemy() : Super()
{
	
}

void AAIControllerStandardEnemy::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
	AStandardEnemy* Char = Cast<AStandardEnemy>(InPawn);
	if (Char && Char->GetBehavior())
	{
		this->BlackboardComp->InitializeBlackboard(*Char->GetBehavior()->BlackboardAsset);
		this->PlayerKeyId = this->BlackboardComp->GetKeyID("Player");
		this->BehaviorTreeComp->StartTree(*Char->GetBehavior());
	}
}