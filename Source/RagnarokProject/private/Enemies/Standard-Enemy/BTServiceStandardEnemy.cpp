#include "BTServiceStandardEnemy.h"
#include "AIControllerStandardEnemy.h"
#include "StandardEnemy.h"

UBTServiceStandardEnemy::UBTServiceStandardEnemy() : Super()
{
	
}

void UBTServiceStandardEnemy::TickNode(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory, float DeltaSeconds)
{
	AAIControllerStandardEnemy* EnemyPC = Cast<AAIControllerStandardEnemy>(OwnerComp.GetAIOwner());
	if (EnemyPC)
	{
		ARagnarokProjectCharacter* Player = Cast<ARagnarokProjectCharacter>(this->GetWorld()->GetFirstPlayerController()->GetPawn());
		if (Player)
		{
			OwnerComp.GetBlackboardComponent()->SetValue<UBlackboardKeyType_Object>(EnemyPC->PlayerKeyId, Player);
		}
	}
}