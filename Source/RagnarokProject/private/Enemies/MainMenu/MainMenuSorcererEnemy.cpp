#include "MainMenuSorcererEnemy.h"
#include "EngineMinimal.h"

AMainMenuSorcererEnemy::AMainMenuSorcererEnemy() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/RagnarokProject/Enemies/SorcererEnemy/Model/SorcererEnemy.SorcererEnemy'"));

	if (MeshAsset.Succeeded())
	{
		this->GetMesh()->SetSkeletalMesh(MeshAsset.Object);
		this->GetMesh()->SetWorldTransform(FTransform(FVector(-20.f, 0.f, -80.f)));
	}

	auto PhysicsAsset = ConstructorHelpers::FObjectFinder<UPhysicsAsset>(TEXT("PhysicsAsset'/Game/RagnarokProject/Enemies/SorcererEnemy/Model/SorcererEnemy_PhysicsAsset.SorcererEnemy_PhysicsAsset'"));
	if (PhysicsAsset.Succeeded())
	{
		this->GetMesh()->SetPhysicsAsset(PhysicsAsset.Object);
		this->GetMesh()->SetSimulatePhysics(false);
	}

	auto AnimationBlueprintAsset = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Menus/MainMenu/SorcererEnemy/AnimBPMainMenuSorcererEnemy.AnimBPMainMenuSorcererEnemy_C'"));
	if (AnimationBlueprintAsset.Succeeded())
	{
		this->AnimationBlueprint = AnimationBlueprintAsset.Object;
		this->GetMesh()->SetAnimInstanceClass(this->AnimationBlueprint);
	}	
}

void AMainMenuSorcererEnemy::BeginPlay()
{
	Super::BeginPlay();	
}