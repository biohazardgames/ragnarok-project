#include "MainMenuStandardEnemy.h"
#include "EngineMinimal.h"

AMainMenuStandardEnemy::AMainMenuStandardEnemy() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/RagnarokProject/Enemies/StandardEnemy/Model/StandardEnemy.StandardEnemy'"));

	if (MeshAsset.Succeeded())
	{
		this->GetMesh()->SetSkeletalMesh(MeshAsset.Object);
		this->GetMesh()->SetWorldTransform(FTransform(FVector(-20.f, 0.f, -80.f)));
	}
	
	auto PhysicsAsset = ConstructorHelpers::FObjectFinder<UPhysicsAsset>(TEXT("PhysicsAsset'/Game/RagnarokProject/Enemies/StandardEnemy/Model/StandardEnemy_PhysicsAsset.StandardEnemy_PhysicsAsset'"));
	if (PhysicsAsset.Succeeded())
	{
		this->GetMesh()->SetPhysicsAsset(PhysicsAsset.Object);
		this->GetMesh()->SetSimulatePhysics(false);
	}

	auto AnimationBlueprintAsset = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Menus/MainMenu/StandardEnemy/AnimBPMainMenuStandardEnemy.AnimBPMainMenuStandardEnemy_C'"));
	if (AnimationBlueprintAsset.Succeeded())
	{
		this->AnimationBlueprint = AnimationBlueprintAsset.Object;
		this->GetMesh()->SetAnimInstanceClass(this->AnimationBlueprint);
	}	
}

void AMainMenuStandardEnemy::BeginPlay()
{
	Super::BeginPlay();	
}