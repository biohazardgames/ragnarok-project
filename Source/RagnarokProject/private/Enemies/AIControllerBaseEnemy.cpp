#include "AIControllerBaseEnemy.h"

AAIControllerBaseEnemy::AAIControllerBaseEnemy()
{
	this->BlackboardComp = CreateDefaultSubobject<UBlackboardComponent>(TEXT("BlackboardComp"));
	this->BehaviorTreeComp = CreateDefaultSubobject<UBehaviorTreeComponent>(TEXT("BehaviorTreeComp"));
}

void AAIControllerBaseEnemy::Possess(APawn* InPawn)
{
	Super::Possess(InPawn);
}