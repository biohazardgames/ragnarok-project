#include "BTTaskFollowPlayerBaseEnemy.h"
#include "BaseEnemy.h"
#include "AIControllerBaseEnemy.h"

EBTNodeResult::Type UBTTaskFollowPlayerBaseEnemy::ExecuteTask(UBehaviorTreeComponent& OwnerComp, uint8* NodeMemory)
{
	AAIControllerBaseEnemy* CharPC = Cast<AAIControllerBaseEnemy>(OwnerComp.GetAIOwner());
	ARagnarokProjectCharacter* Player = Cast<ARagnarokProjectCharacter>(OwnerComp.GetBlackboardComponent()->GetValue<UBlackboardKeyType_Object>(CharPC->PlayerKeyId));
	
	if (Player && CharPC)
	{
		FString Class = CharPC->GetPawn()->GetClass()->GetName();
		ABaseEnemy* BaseEnemy = Cast<ABaseEnemy>(CharPC->GetPawn());

		if (BaseEnemy)
		{
			BaseEnemy->GetMesh()->SetWorldRotation
			(
				FRotationMatrix::MakeFromX(Player->GetActorLocation() - CharPC->GetPawn()->GetActorLocation()).Rotator(),
				false,
				nullptr,
				ETeleportType::TeleportPhysics
			);

			CharPC->MoveToActor
			(
				Player, 
				BaseEnemy->GetCommonState()->GetMinDistanceToPlayer(Class),
				true,
				true,
				true,
				0, 
				true);
			return EBTNodeResult::Succeeded;
		}
	}
	return EBTNodeResult::Failed;
}




