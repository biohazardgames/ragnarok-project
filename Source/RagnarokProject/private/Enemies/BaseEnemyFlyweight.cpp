#include "BaseEnemyFlyweight.h"
#include "RagnarokProjectCharacter.h"
#include "EnemyManager.h"

ABaseEnemyFlyweight::ABaseEnemyFlyweight()
{
	this->PrimaryActorTick.bCanEverTick = false;

	auto Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Enemies/Sounds/Death.Death'"));

	if (Audio.Succeeded())
	{
		this->DeathSound = Audio.Object;
	}
}

void ABaseEnemyFlyweight::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Players;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), ARagnarokProjectCharacter::StaticClass(), Players);
	this->Player = Cast<ARagnarokProjectCharacter>(Players[0]);

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyManager::StaticClass(), Managers);
	this->EnemyManager = Cast<AEnemyManager>(Managers[0]);

	this->EnemyMinDistanceToPlayer.Emplace("StandardEnemy", 0.f);
	this->EnemyMinDistanceToPlayer.Emplace("SorcererEnemy", 30.f);
}

ARagnarokProjectCharacter* ABaseEnemyFlyweight::GetPlayer()
{
	return this->Player;
}

AEnemyManager* ABaseEnemyFlyweight::GetEnemyManager()
{
	return this->EnemyManager;
}

float ABaseEnemyFlyweight::GetDamage()
{
	return this->Damage;
}

float ABaseEnemyFlyweight::GetEarnedMoney()
{
	return this->EarnedMoney;
}

float ABaseEnemyFlyweight::GetAttackCooldown()
{
	return this->AttackCooldown;
}

float ABaseEnemyFlyweight::GetSpeed()
{
	return this->Speed;
}

USoundBase* ABaseEnemyFlyweight::GetRunningSound()
{
	return this->RunningSound;
}

USoundBase* ABaseEnemyFlyweight::GetAttackSound()
{
	return this->AttackSound;
}

USoundBase* ABaseEnemyFlyweight::GetDeathSound()
{
	return this->DeathSound;
}

USoundBase* ABaseEnemyFlyweight::GetLaunchProjectileSound()
{
	return this->LaunchProjectileSound;
}

float ABaseEnemyFlyweight::GetMinDistanceToPlayer(FString Class)
{
	return (this->EnemyMinDistanceToPlayer.Contains(Class))
		? this->EnemyMinDistanceToPlayer[Class]
		: DEFAULT_MIN_DISTANCE_TO_PLAYER;
}

UClass* ABaseEnemyFlyweight::GetAnimationBlueprint()
{
	return this->AnimationBlueprint;
}

TArray<UAnimMontage*> ABaseEnemyFlyweight::GetAttackAnimations()
{
	return this->AttackAnimations;
}

TArray<UAnimMontage*> ABaseEnemyFlyweight::GetDeathAnimations()
{
	return this->DeathAnimations;
}

