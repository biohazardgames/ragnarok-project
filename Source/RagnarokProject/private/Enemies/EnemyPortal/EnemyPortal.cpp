#include "EnemyPortal.h"
#include "EnemyManager.h"
#include "EnemyPortalFlyweight.h"
#include "BaseEnemy.h"
#include "StandardEnemy.h"
#include "SorcererEnemy.h"

AEnemyPortal::AEnemyPortal()
{
	this->Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("PortalMesh"));
	
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(1.2f, 1.2f, 1.2f));
		this->Mesh->SetupAttachment(this->RootComponent);
		this->Mesh->bHiddenInGame = true;
		this->Mesh->SetCollisionResponseToAllChannels(ECR_Ignore);
		this->RootComponent = this->Mesh;
	}
	
	auto ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Enemies/EnemyPortal/PortalParticleSystem.PortalParticleSystem'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->PortalFX = ParticleSystemAsset.Object;
	}
	
	this->LastEnemyTypeSpawnedKey = NULL;
	this->DistanceToPortal = 700.f;
}

void AEnemyPortal::BeginPlay()
{
	Super::BeginPlay();

	UGameplayStatics::SpawnEmitterAttached(this->PortalFX.Get(), this->Mesh);
}

void AEnemyPortal::StartGame()
{
	TArray<AActor*> Flyweights;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyPortalFlyweight::StaticClass(), Flyweights);
	this->CommonState = Cast<AEnemyPortalFlyweight>(Flyweights[0]);
}

void AEnemyPortal::SpawnEnemy(bool IsBossWave)
{	
	int EnemyTypeKey;
	FVector Location = this->GetActorLocation() + this->GetActorRightVector() * this->DistanceToPortal;

	if (!IsBossWave)
	{
		EnemyTypeKey = FMath::RandRange(0, this->CommonState->GetNumberOfEnemyTypes() - 1);
		this->LastEnemyTypeSpawnedKey = (EnemyTypeKey != this->LastEnemyTypeSpawnedKey)
			? EnemyTypeKey
			: FMath::RandRange(0, this->CommonState->GetNumberOfEnemyTypes() - 1);

		this->GetWorld()->SpawnActor(this->CommonState->GetEnemyClass(this->LastEnemyTypeSpawnedKey), &Location);
	}
	else
	{
		EnemyTypeKey = FMath::RandRange(0, this->CommonState->GetNumberOfBossTypes() - 1);
		this->LastEnemyTypeSpawnedKey = (EnemyTypeKey != this->LastEnemyTypeSpawnedKey)
			? EnemyTypeKey
			: FMath::RandRange(0, this->CommonState->GetNumberOfBossTypes() - 1);

		this->GetWorld()->SpawnActor(this->CommonState->GetBossClass(this->LastEnemyTypeSpawnedKey), &Location);
	}							
}