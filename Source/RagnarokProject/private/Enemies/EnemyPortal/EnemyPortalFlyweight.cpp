#include "EnemyPortalFlyweight.h"
#include "EnemyManager.h"
#include "BaseEnemy.h"
#include "StandardEnemy.h"
#include "SorcererEnemy.h"

AEnemyPortalFlyweight::AEnemyPortalFlyweight()
{
	this->PrimaryActorTick.bCanEverTick = false;
	
	this->EnemyClasses.Emplace(0, AStandardEnemy::StaticClass());
	this->BossClasses.Emplace(0, ASorcererEnemy::StaticClass());
}

void AEnemyPortalFlyweight::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyManager::StaticClass(), Managers);
	this->Manager = Cast<AEnemyManager>(Managers[0]);
}

AEnemyManager* AEnemyPortalFlyweight::GetManager()
{
	return this->Manager;
}

TSubclassOf<ABaseEnemy> AEnemyPortalFlyweight::GetEnemyClass(int EnemyClassKey)
{
	return this->EnemyClasses[EnemyClassKey];
}

int AEnemyPortalFlyweight::GetNumberOfEnemyTypes()
{
	return this->EnemyClasses.Num();
}

TSubclassOf<ABaseEnemy> AEnemyPortalFlyweight::GetBossClass(int BossClassKey)
{
	return this->BossClasses[BossClassKey];
}

int AEnemyPortalFlyweight::GetNumberOfBossTypes()
{
	return this->BossClasses.Num();
}
