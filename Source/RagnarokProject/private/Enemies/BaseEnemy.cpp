#include "BaseEnemy.h"
#include "RagnarokProjectCharacter.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "TimerManager.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/CollisionProfile.h"
#include "Sound/SoundBase.h"
#include "BaseEnemyFlyweight.h"
#include "EnemyFactory.h"
#include "EnemyManager.h"
#include "TurretProjectile.h"
#include "AutomaticTurret.h"

ABaseEnemy::ABaseEnemy() : Super()
{
	this->PrimaryActorTick.bCanEverTick = false;

	this->GetMesh()->SetCollisionObjectType(COLLISION_ENEMY);
	this->GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	this->GetMesh()->SetCollisionResponseToChannel(COLLISION_TRIGGER_COMPONENT, ECR_Ignore);
	this->GetMesh()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Ignore);

	this->GetCapsuleComponent()->SetGenerateOverlapEvents(true);
	this->GetCapsuleComponent()->SetCollisionObjectType(COLLISION_ENEMY);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Pawn, ECR_Overlap);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Block);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_TRIGGER_COMPONENT, ECR_Overlap);
	this->GetCapsuleComponent()->OnComponentBeginOverlap.AddDynamic(this, &ABaseEnemy::OnOverlapBegin);
	this->GetCapsuleComponent()->OnComponentEndOverlap.AddDynamic(this, &ABaseEnemy::OnOverlapEnd);

	this->CanAttack = true;
	this->IsNearPlayer = false;
	this->Dead = false;

	this->HealthShield = CreateDefaultSubobject<UEnemyHealthShieldComponent>(TEXT("HealthShieldComponent"));
}

// Called when the game starts or when spawned
void ABaseEnemy::BeginPlay()
{
	Super::BeginPlay();	
	
	TArray<AActor*> EnemyFactories;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), AEnemyFactory::StaticClass(), EnemyFactories);

	this->Factory = Cast<AEnemyFactory>(EnemyFactories[0]);
}

void ABaseEnemy::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{

	if (OtherActor && OtherActor != this && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		this->IsNearPlayer = true;
	}
}

void ABaseEnemy::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{

	if (OtherActor && OtherActor != this && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		this->IsNearPlayer = false;
	}
}

void ABaseEnemy::Attack(AActor* Target)
{
	this->CanAttack = false;

	this->GetWorldTimerManager().ClearTimer(this->Timer);
	this->GetWorldTimerManager().SetTimer
	(
		this->Timer,
		this, 
		&ABaseEnemy::EnableCanAttack, 
		this->PlayAnimation(
			this->CommonState->GetAttackAnimations()[FMath::RandRange(0, this->CommonState->GetAttackAnimations().Num() - 1)]
		) + this->CommonState->GetAttackCooldown(),
		false
	);

	this->PlaySound(this->CommonState->GetAttackSound());
}

void ABaseEnemy::Death(AActor* DamageCauser)
{
	if (!this->Dead)
	{
		if (DamageCauser && DamageCauser->IsA(ATurretProjectile::StaticClass()))
		{
			Cast<AAutomaticTurret>(
					Cast<ATurretProjectile>(DamageCauser)->GetOwner()
			)->TargetKilled(this->GetUniqueID());
		}

		this->GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
		this->CanAttack = false;
		this->GetCharacterMovement()->MaxWalkSpeed = 0.f;

		this->GetWorldTimerManager().ClearTimer(this->Timer);
		this->GetWorldTimerManager().SetTimer
		(
			this->Timer,
			this,
			&ABaseEnemy::DeathFinished,
			this->PlayAnimation(
				this->CommonState->GetDeathAnimations()[FMath::RandRange(0, this->CommonState->GetDeathAnimations().Num() - 1)]
			) - 0.5f,
			false
		);

		this->PlaySound(this->CommonState->GetDeathSound());
		this->CommonState->GetEnemyManager()->EnemyKilled(this->CommonState->GetEarnedMoney());
		this->Dead = true;
	}
}

float ABaseEnemy::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

UBehaviorTree* ABaseEnemy::GetBehavior()
{
	return this->Behavior;
}

ABaseEnemyFlyweight* ABaseEnemy::GetCommonState()
{
	return this->CommonState;
}

bool ABaseEnemy::GetCanAttack()
{
	return this->CanAttack;
}

bool ABaseEnemy::GetIsNearPlayer()
{
	return this->IsNearPlayer;
}

void ABaseEnemy::EnableCanAttack()
{
	this->CanAttack = true;
}

float ABaseEnemy::PlayAnimation(UAnimMontage* Animation, float InPlayRate, FName StartSectionName)
{
	return (Animation) ? this->PlayAnimMontage(Animation, InPlayRate, StartSectionName) : 0.f;
}

void ABaseEnemy::PlaySound(USoundBase* Sound)
{
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation
		(
			this,
			Sound,
			this->GetActorLocation()
		);
	}
}

void ABaseEnemy::DeathFinished()
{
	this->Destroy();
}



