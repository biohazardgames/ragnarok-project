// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "RagnarokProjectCharacter.h"
#include "HeadMountedDisplayFunctionLibrary.h"
#include "Camera/CameraComponent.h"
#include "Components/CapsuleComponent.h"
#include "Components/InputComponent.h"
#include "GameFramework/CharacterMovementComponent.h"
#include "GameFramework/Controller.h"
#include "GameFramework/SpringArmComponent.h"
#include "AssaultRifle.h"
#include "LaserCannon.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "EngineGlobals.h"
#include "Engine/Engine.h" 
#include "PurchasableObject.h"
#include "PurchasableObjectsManager.h"
#include "RagnarokProject.h"
#include "EnemyManager.h"
#include "HUDManager.h"
#include "PauseMenuManager.h"
#include "BaseSkill.h"
#include "HealthFontSkill.h"
#include "GameplayManager.h"

//////////////////////////////////////////////////////////////////////////
// ARagnarokProjectCharacter

ARagnarokProjectCharacter::ARagnarokProjectCharacter() : Super()
{
	this->PrimaryActorTick.bCanEverTick = true;

	this->GetMesh()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->GetMesh()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	
	// Set size for collision capsule
	this->GetCapsuleComponent()->InitCapsuleSize(42.f, 96.0f);
	this->GetCapsuleComponent()->SetGenerateOverlapEvents(true);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->GetCapsuleComponent()->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);

	// set our turn rates for input
	this->BaseTurnRate = 45.f;
	this->BaseLookUpRate = 45.f;

	// Don't rotate when the controller rotates. Let that just affect the camera.
	this->bUseControllerRotationPitch = false;
	this->bUseControllerRotationYaw = true;
	this->bUseControllerRotationRoll = true;

	// Configure character movement
	this->GetCharacterMovement()->RotationRate = FRotator(0.0f, 540.0f, 0.0f); // ...at this rotation rate
	this->GetCharacterMovement()->JumpZVelocity = 600.f;
	this->GetCharacterMovement()->AirControl = 0.2f;
	this->GetCharacterMovement()->GravityScale = 1.5f;
	this->GetCharacterMovement()->bCanWalkOffLedgesWhenCrouching = true;

	// Create a camera boom (pulls in towards the player if there is a collision)
	this->CameraBoom = CreateDefaultSubobject<USpringArmComponent>(TEXT("CameraBoom"));
	this->CameraBoom->bUsePawnControlRotation = true;
	this->CameraBoom->SetRelativeLocation(FVector(90.f, 60.f, 80.f));
	this->CameraBoom->SetupAttachment(RootComponent);

	// Create a follow camera
	this->FollowCamera = CreateDefaultSubobject<UCameraComponent>(TEXT("FollowCamera"));
	this->FollowCamera->SetupAttachment(CameraBoom);
	
	this->WeaponAttachSocketName = "WeaponSocket";
	this->BackWeaponAttachSocketName = "BackSocket";
	this->DefaultFOV = FollowCamera->FieldOfView;
	this->ZoomedFOV = 65.f;
	this->ZoomInterpSpeed = 20.f;
	
	auto MeshAsset = ConstructorHelpers::FObjectFinder<USkeletalMesh>(TEXT("SkeletalMesh'/Game/RagnarokProject/Player/Soldier/Model/ely_k_atienza.ely_k_atienza'"));
	
	if (MeshAsset.Succeeded())
	{
		this->GetMesh()->SetSkeletalMesh(MeshAsset.Object);
		this->GetMesh()->SetWorldTransform(FTransform(FVector(10.f, 0.f, -80.f)));
		this->GetMesh()->SetRelativeRotation(FRotator(0.0f, 270.0f, 0.0f));
	}

	auto AnimationBlueprintAsset = ConstructorHelpers::FObjectFinder<UClass>(TEXT("AnimBlueprint'/Game/RagnarokProject/Player/Soldier/Animations/RPAnimBP.RPAnimBP_C'"));
	if (AnimationBlueprintAsset.Succeeded())
	{
		this->AnimationBlueprint = AnimationBlueprintAsset.Object;
		this->GetMesh()->SetAnimInstanceClass(this->AnimationBlueprint);
	}

	auto AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Player/Soldier/Animations/Equip_Rifle_Standing_Montage.Equip_Rifle_Standing_Montage'"));
	if (AnimationMontage.Succeeded())
	{
		this->EquipAnimation = AnimationMontage.Object;
	}

	AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Player/Soldier/Animations/AnimMontageDeath.AnimMontageDeath'"));
	if (AnimationMontage.Succeeded())
	{
		this->DeathAnimation = AnimationMontage.Object;
	}

	auto SoundAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/RagnarokProject/Player/Sounds/HurtCue.HurtCue'"));
	if (SoundAsset.Succeeded())
	{
		this->HurtSound = SoundAsset.Object;
	}

	this->PurchasableObject = NULL;
	this->Money = 0.f;
	this->CanFire = false;
	this->CanReload = true;
	this->CanAim = true;
	this->CanSwapWeapons = true;
	this->BaseSpeed = 500.f;
	this->SpeedModifier = 1.75f;

	this->CrouchKeyPressed = false;
	this->SprintKeyPressed = false;
	this->ReloadKeyPressed = false;
	this->FireKeyPressed = false;
	this->AimKeyPressed = false;
	
	this->GetCharacterMovement()->MaxWalkSpeed = this->BaseSpeed;
	this->GetCharacterMovement()->MaxWalkSpeedCrouched = this->BaseSpeed / this->SpeedModifier;
	this->HealthShield = CreateDefaultSubobject<UPlayerHealthShieldComponent>(TEXT("HealthShieldComponent"));

	this->CurrentSkill = AHealthFontSkill::StaticClass();
	this->CanUseSkill = false;
}

void ARagnarokProjectCharacter::BeginPlay()
{
	Super::BeginPlay();

	// Spawn a default weapon
	FActorSpawnParameters SpawnParams;
	SpawnParams.SpawnCollisionHandlingOverride = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;

	this->CurrentWeaponSlot = 0;

	this->CurrentWeapon = this->GetWorld()->SpawnActor<AWeapon>(AAssaultRifle::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (this->CurrentWeapon)
	{
		this->CurrentWeapon->SetOwner(this);
		this->CurrentWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, this->WeaponAttachSocketName);
	}

	this->BackWeapon = this->GetWorld()->SpawnActor<AWeapon>(ALaserCannon::StaticClass(), FVector::ZeroVector, FRotator::ZeroRotator, SpawnParams);
	if (this->BackWeapon)
	{
		this->BackWeapon->SetOwner(this);
		this->BackWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, this->BackWeaponAttachSocketName);
	}
	
	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), APauseMenuManager::StaticClass(), Managers);
	this->PauseMenuManager = Cast<APauseMenuManager>(Managers[0]);

	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AEnemyManager::StaticClass(), Managers);
	Cast<AEnemyManager>(Managers[0])->OnEarnMoney.AddDynamic(this, &ARagnarokProjectCharacter::UpdateMoney);
	
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), APurchasableObjectsManager::StaticClass(), Managers);
	Cast<APurchasableObjectsManager>(Managers[0])->OnPurchaseAmmo.AddDynamic(this, &ARagnarokProjectCharacter::PurchasedAmmo);
	
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AGameplayManager::StaticClass(), Managers);
	Cast<AGameplayManager>(Managers[0])->OnStartGame.AddDynamic(this, &ARagnarokProjectCharacter::StartGame);
}

void ARagnarokProjectCharacter::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	this->FollowCamera->SetFieldOfView
	(
		FMath::FInterpTo
		(
			this->FollowCamera->FieldOfView,
			(this->AimKeyPressed && !this->SprintKeyPressed) ? this->ZoomedFOV : this->DefaultFOV,
			DeltaTime,
			this->ZoomInterpSpeed
		)
	);
}

//////////////////////////////////////////////////////////////////////////
// Input

void ARagnarokProjectCharacter::SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);
	
	PlayerInputComponent->BindAction("Crouch", IE_Pressed, this, &ARagnarokProjectCharacter::Crouch);
	PlayerInputComponent->BindAction("Crouch", IE_Released, this, &ARagnarokProjectCharacter::StopCrouching);

	PlayerInputComponent->BindAction("Reload", IE_Pressed, this, &ARagnarokProjectCharacter::Reload);

	PlayerInputComponent->BindAction("Sprint", IE_Pressed, this, &ARagnarokProjectCharacter::Sprint);
	PlayerInputComponent->BindAction("Sprint", IE_Released, this, &ARagnarokProjectCharacter::StopSprinting);

	PlayerInputComponent->BindAction("Aim", IE_Pressed, this, &ARagnarokProjectCharacter::Aim);
	PlayerInputComponent->BindAction("Aim", IE_Released, this, &ARagnarokProjectCharacter::StopAiming);

	PlayerInputComponent->BindAction("Fire", IE_Pressed, this, &ARagnarokProjectCharacter::Fire);
	PlayerInputComponent->BindAction("Fire", IE_Released, this, &ARagnarokProjectCharacter::StopFiring);

	PlayerInputComponent->BindAction("Interact", IE_Pressed, this, &ARagnarokProjectCharacter::Interact);

	PlayerInputComponent->BindAction("EquipPrimaryWeapon", IE_Pressed, this, &ARagnarokProjectCharacter::EquipPrimaryWeapon);

	PlayerInputComponent->BindAction("EquipSecondaryWeapon", IE_Pressed, this, &ARagnarokProjectCharacter::EquipSecondaryWeapon);

	PlayerInputComponent->BindAction("Pause", IE_Pressed, this, &ARagnarokProjectCharacter::Pause).bExecuteWhenPaused = true;

	PlayerInputComponent->BindAction("UseSkill", IE_Pressed, this, &ARagnarokProjectCharacter::UseSkill);

	PlayerInputComponent->BindAxis("MoveForward", this, &ARagnarokProjectCharacter::MoveForward);
	PlayerInputComponent->BindAxis("MoveRight", this, &ARagnarokProjectCharacter::MoveRight);
	PlayerInputComponent->BindAxis("LookUp", this, &ARagnarokProjectCharacter::AddControllerPitchInput);
	PlayerInputComponent->BindAxis("Turn", this, &ARagnarokProjectCharacter::AddControllerYawInput);
}

void ARagnarokProjectCharacter::TurnAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	this->AddControllerYawInput(Rate * BaseTurnRate * GetWorld()->GetDeltaSeconds());
}

void ARagnarokProjectCharacter::LookUpAtRate(float Rate)
{
	// calculate delta for this frame from the rate information
	this->AddControllerPitchInput(Rate * BaseLookUpRate * GetWorld()->GetDeltaSeconds());
}

void ARagnarokProjectCharacter::Crouch()
{
	if (this->Controller && !this->SprintKeyPressed)
	{
		this->CrouchKeyPressed = true;
		this->CameraBoom->SetRelativeLocation(FVector(90.f, 60.f, 40.f));
		this->GetCharacterMovement()->MaxWalkSpeed = this->BaseSpeed / this->SpeedModifier;
		this->CurrentWeapon->SetBulletSpreadByPlayerPosition("Crouching");
		this->BackWeapon->SetBulletSpreadByPlayerPosition("Crouching");
	}
}

void ARagnarokProjectCharacter::StopCrouching()
{
	if (this->Controller)
	{
		this->CrouchKeyPressed = false;
		this->CameraBoom->SetRelativeLocation(FVector(90.f, 60.f, 80.f));
		this->GetCharacterMovement()->MaxWalkSpeed = this->BaseSpeed;
		this->CurrentWeapon->SetBulletSpreadByPlayerPosition("Standing");
		this->BackWeapon->SetBulletSpreadByPlayerPosition("Standing");
	}
}

void ARagnarokProjectCharacter::Reload()
{
	if (this->Controller && this->CurrentWeapon && this->CanReload)
	{
		this->ReloadKeyPressed = true;
		this->CanFire = false;
		this->CanReload = false;
		this->CanSwapWeapons = false;
		this->CurrentWeapon->StartReload();
	}
}

void ARagnarokProjectCharacter::StopReloading()
{
	if (this->Controller)
	{
		this->ReloadKeyPressed = false;
		this->CanFire = (this->SprintKeyPressed)
			? false
			: true;
		this->CanSwapWeapons = true;
		this->CanReload = true;
	}
}

void ARagnarokProjectCharacter::Sprint()
{
	if (this->Controller)
	{
		this->SprintKeyPressed = true;
		this->CanFire = false;
		this->CanAim = false;
		this->GetCharacterMovement()->MaxWalkSpeed = this->BaseSpeed * this->SpeedModifier;
	}
}

void ARagnarokProjectCharacter::StopSprinting()
{
	if (this->Controller)
	{
		this->SprintKeyPressed = false;
		this->CanFire = true;
		this->CanAim = true;
		this->GetCharacterMovement()->MaxWalkSpeed = this->BaseSpeed;
	}
}

void ARagnarokProjectCharacter::Aim()
{
	if (this->Controller && this->CanAim)
	{
		this->AimKeyPressed = true;
	}
}

void ARagnarokProjectCharacter::StopAiming()
{
	if (this->Controller)
	{
		this->AimKeyPressed = false;
	}
}

void ARagnarokProjectCharacter::Fire()
{
	if (this->Controller && this->CurrentWeapon && this->CanFire)
	{
		this->FireKeyPressed = true;
		this->CurrentWeapon->StartFire();
		this->CanReload = false;
		this->CanSwapWeapons = false;
	}
}

void ARagnarokProjectCharacter::StopFiring()
{
	if (this->Controller)
	{
		this->CurrentWeapon->StopFire();
		this->FireKeyPressed = false;
		this->CanReload = true;
		this->CanSwapWeapons = true;
	}
}

void ARagnarokProjectCharacter::Interact()
{
	if (this->Controller && this->PurchasableObject)
	{
		this->UpdateMoney(this->PurchasableObject->PurchaseObject(this->Money));
	}
}

void ARagnarokProjectCharacter::UseSkill()
{
	if (this->Controller && this->CanUseSkill)
	{
		this->CanUseSkill = false;
		this->OnUpdateSkillState.Broadcast(this->CanUseSkill);

		FVector Location = this->GetActorLocation() + this->GetActorForwardVector() * 60.f;
		Location.Z = 0.f;
		this->GetWorld()->SpawnActor(this->CurrentSkill, &Location);
	}
}

void ARagnarokProjectCharacter::UpdateMoney(float Money)
{
	if (Money != 0.f)
	{
		this->Money += Money;
		this->OnUpdateMoney.Broadcast(this->Money);
	}
}

void ARagnarokProjectCharacter::PurchasedAmmo(int PurchasedAmmo)
{
	this->CurrentWeapon->UpdateAmmo(0.f, PurchasedAmmo);
}

void ARagnarokProjectCharacter::EquipPrimaryWeapon()
{
	if (this->Controller && !this->CurrentWeapon->IsA(AAssaultRifle::StaticClass()) && this->CanSwapWeapons)
	{
		this->CurrentWeaponSlot = 0;
		this->SwapWeapons();
	}
}

void ARagnarokProjectCharacter::EquipSecondaryWeapon()
{
	if (this->Controller && !this->CurrentWeapon->IsA(ALaserCannon::StaticClass()) && this->CanSwapWeapons)
	{
		this->CurrentWeaponSlot = 1;
		this->SwapWeapons();
	}
}

void ARagnarokProjectCharacter::Pause()
{
	APlayerController* const PlayerController = Cast<APlayerController>(GEngine->GetFirstLocalPlayerController(GetWorld()));
	if (PlayerController)
	{
		this->PauseMenuManager->SetGamePaused();
	}
}

bool ARagnarokProjectCharacter::GetCrouchKeyPressed()
{
	return this->CrouchKeyPressed;
}

bool ARagnarokProjectCharacter::GetReloadKeyPressed()
{
	return this->ReloadKeyPressed;
}

bool ARagnarokProjectCharacter::GetSprintKeyPressed()
{
	return this->SprintKeyPressed;
}

bool ARagnarokProjectCharacter::GetAimKeyPressed()
{
	return this->AimKeyPressed;
}

bool ARagnarokProjectCharacter::GetFireKeyPressed()
{
	return this->FireKeyPressed;
}

void ARagnarokProjectCharacter::MoveForward(float Value)
{
	if ((this->Controller) && (Value != 0.f))
	{
		this->AddMovementInput(this->GetActorForwardVector() * Value);
	}
}

void ARagnarokProjectCharacter::MoveRight(float Value)
{
	if ((this->Controller) && (Value != 0.f) )
	{
		this->AddMovementInput(this->GetActorRightVector() * Value);
	}
}

FVector ARagnarokProjectCharacter::GetPawnViewLocation() const
{
	if (this->FollowCamera)
	{
		return this->FollowCamera->GetComponentLocation();
	}

	return Super::GetPawnViewLocation();
}

FRotator ARagnarokProjectCharacter::GetAimOffsets() const
{
	return this->ActorToWorld().InverseTransformVectorNoScale(GetBaseAimRotation().Vector()).Rotation();
}

void ARagnarokProjectCharacter::SetPurchasableObject(APurchasableObject* Object)
{
	this->PurchasableObject = Object;
}

void ARagnarokProjectCharacter::SwapWeapons()
{
	this->CanFire = false;
	this->CanReload = false;
	this->CanSwapWeapons = false;
	
	this->GetWorldTimerManager().SetTimer
	(
		this->Timer,
		this,
		&ARagnarokProjectCharacter::SwapWeaponsFinished,
		this->PlayAnimMontage(this->EquipAnimation),
		false
	);
}

void ARagnarokProjectCharacter::SwapWeaponsFinished()
{
	if (!this->SprintKeyPressed)
	{
		this->CanFire = true;
		this->CanReload = true;
		this->CanAim = true;		
	}	

	this->CanSwapWeapons = true;
}

void ARagnarokProjectCharacter::SwapWeaponMeshes()
{
	AWeapon* TemporalWeapon = this->CurrentWeapon;
	this->CurrentWeapon = this->BackWeapon;
	this->BackWeapon = TemporalWeapon;
	this->CurrentWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, this->WeaponAttachSocketName);
	this->BackWeapon->AttachToComponent(this->GetMesh(), FAttachmentTransformRules::SnapToTargetNotIncludingScale, this->BackWeaponAttachSocketName);

	this->OnUpdateAmmo.Broadcast(this->CurrentWeapon->GetAmmoInClip(), this->CurrentWeapon->GetCurrentAmmo());
}

int ARagnarokProjectCharacter::getCurrentWeaponSlot()
{
	return this->CurrentWeaponSlot;
}

AWeapon* ARagnarokProjectCharacter::GetCurrentWeapon()
{
	return this->CurrentWeapon;
}

UPlayerHealthShieldComponent* ARagnarokProjectCharacter::GetHealthShieldComponent()
{
	return this->HealthShield;
}

void ARagnarokProjectCharacter::SetCanFire(bool CanFire)
{
	this->CanFire = CanFire;
}

void ARagnarokProjectCharacter::SetCanReload(bool CanReload)
{
	this->CanReload = CanReload;
}

void ARagnarokProjectCharacter::SetCanAim(bool CanAim)
{
	this->CanAim = CanAim;
}

void ARagnarokProjectCharacter::SetCanSwapWeapons(bool CanSwapWeapons)
{
	this->CanSwapWeapons = CanSwapWeapons;
}

float ARagnarokProjectCharacter::TakeDamage(float Damage, struct FDamageEvent const& DamageEvent, class AController* EventInstigator, class AActor* DamageCauser)
{
	this->PlayCharacterSound(this->HurtSound);
	return Super::TakeDamage(Damage, DamageEvent, EventInstigator, DamageCauser);
}

void ARagnarokProjectCharacter::Death()
{
	this->CanFire = false;
	this->CanReload = false;
	this->GetCharacterMovement()->MaxWalkSpeed = 0.f;
	this->OnDiePlayer.Broadcast();	
}

void ARagnarokProjectCharacter::PlayCharacterSound(USoundCue* SoundToPlay)
{
	if (this->GetOwner() && SoundToPlay)
	{
		UGameplayStatics::SpawnSoundAttached(SoundToPlay, this->GetOwner()->GetRootComponent());
	}
}

void ARagnarokProjectCharacter::AmmoUpdated(int ClipAmmo, int Ammo)
{
	this->OnUpdateAmmo.Broadcast(ClipAmmo, Ammo);
}

void ARagnarokProjectCharacter::StartSkillCooldown(float CooldownTime)
{
	this->GetWorldTimerManager().SetTimer
	(
		this->SkillTimer,
		this,
		&ARagnarokProjectCharacter::EnableCanUseSkill,
		CooldownTime,
		false
	);
}

void ARagnarokProjectCharacter::EnableCanUseSkill()
{
	this->CanUseSkill = true;
	this->OnUpdateSkillState.Broadcast(this->CanUseSkill);
}

void ARagnarokProjectCharacter::StartGame()
{
	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), AHUDManager::StaticClass(), Managers);
	Cast<AHUDManager>(Managers[0])->UpdateAmmo(this->CurrentWeapon->GetAmmoInClip(), this->CurrentWeapon->GetCurrentAmmo());
	
	this->CanFire = true;
	this->CanUseSkill = true;
}