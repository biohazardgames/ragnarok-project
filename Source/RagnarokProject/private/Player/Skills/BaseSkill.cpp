#include "BaseSkill.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/CollisionProfile.h"

ABaseSkill::ABaseSkill() 
{
	PrimaryActorTick.bCanEverTick = false;

	this->InteractionRadius = CreateDefaultSubobject<USphereComponent>(TEXT("InteractionSphere"));
	this->InteractionRadius->SetCollisionObjectType(COLLISION_TRIGGER_COMPONENT);
	this->RootComponent = this->InteractionRadius;

	this->InteractionRadius->SetGenerateOverlapEvents(true);
	this->InteractionRadius->OnComponentBeginOverlap.AddDynamic(this, &ABaseSkill::OnOverlapBegin);
	this->InteractionRadius->OnComponentEndOverlap.AddDynamic(this, &ABaseSkill::OnOverlapEnd);
	this->InteractionRadius->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	this->InteractionRadius->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	this->Mesh = CreateDefaultSubobject<UStaticMeshComponent>(TEXT("SkillMesh"));
	this->Mesh->SetGenerateOverlapEvents(false);
	this->Mesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	this->Mesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
}

void ABaseSkill::BeginPlay()
{
	Super::BeginPlay();
	
	UGameplayStatics::SpawnEmitterAttached(this->SkillFX.Get(), this->Mesh);
}

void ABaseSkill::PlaySound(USoundBase* Sound)
{
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation
		(
			this,
			Sound,
			this->GetActorLocation()
		);
	}
}

void ABaseSkill::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
}

void ABaseSkill::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
}