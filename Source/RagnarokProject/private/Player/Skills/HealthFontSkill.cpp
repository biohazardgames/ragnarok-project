// Copyright 1998-2017 Epic Games, Inc. All Rights Reserve

#include "HealthFontSkill.h"
#include "UObject/ConstructorHelpers.h"
#include "Engine/StaticMesh.h"

AHealthFontSkill::AHealthFontSkill() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(1.f, 1.f, 1.f));
		this->Mesh->SetupAttachment(this->RootComponent);
		this->Mesh->bHiddenInGame = true;
	}

	this->InteractionRadius->InitSphereRadius(300.f);
	
	auto ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Player/Skills/HealthFont/HealthFont.HealthFont'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->SkillFX = ParticleSystemAsset.Object;
	}

	auto Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Enemies/SorcererEnemy/Sounds/ProjectileImpact.ProjectileImpact'"));

	if (Audio.Succeeded())
	{
		this->SpawnedSound = Audio.Object;
	}
	
	Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/Enemies/SorcererEnemy/Sounds/ProjectileImpact.ProjectileImpact'"));

	if (Audio.Succeeded())
	{
		this->EffectSound = Audio.Object;
	}	
	
	this->EffectQuantity = 15.f;
	this->TimeToFireSkill = 2.f;
	this->CooldownTime = 5.f;
	this->Charges = 3;
}

void AHealthFontSkill::BeginPlay()
{
	Super::BeginPlay();
}


void AHealthFontSkill::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (OtherActor && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		this->SkillTarget = OtherActor;
		this->GetWorldTimerManager().SetTimer
		(
			this->CooldownTimer,
			this,
			&AHealthFontSkill::FireSkill,
			this->TimeToFireSkill,
			true
		);
	}
}

void AHealthFontSkill::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex)
{
	if (OtherActor && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		this->SkillTarget = NULL;
		this->GetWorldTimerManager().ClearTimer(this->CooldownTimer);
	}
}

void AHealthFontSkill::FireSkill() 
{
	if (this->Charges > 0)
	{
		this->Charges -= 1;
		Cast<ARagnarokProjectCharacter>(this->SkillTarget)->GetHealthShieldComponent()->Heal(this->EffectQuantity);
	}
	else
	{
		Cast<ARagnarokProjectCharacter>(this->SkillTarget)->StartSkillCooldown(this->CooldownTime);
		this->Destroy();
	}
}