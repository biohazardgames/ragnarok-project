#include "PlayerHealthShieldComponent.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "RagnarokProjectCharacter.h"
#include "PurchasableObjectsManager.h"

UPlayerHealthShieldComponent::UPlayerHealthShieldComponent() : Super()
{
}

// Called when the game starts
void UPlayerHealthShieldComponent::BeginPlay()
{
	Super::BeginPlay();

	this->OnUpdateHealthShield.Broadcast(this->Health / 100.f, this->Shield / 100.f, false);

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(this->GetWorld(), APurchasableObjectsManager::StaticClass(), Managers);
	Cast<APurchasableObjectsManager>(Managers[0])->OnPurchaseMedicalKit.AddDynamic(this, &UPlayerHealthShieldComponent::Heal);
}

void UPlayerHealthShieldComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy,
	AActor* DamageCauser)
{
	Super::HandleTakeAnyDamage(DamagedActor, Damage, DamageType, InstigatedBy, DamageCauser);

	if (DamageCauser != DamagedActor && DamageCauser != this->GetOwner())
	{
		this->OnUpdateHealthShield.Broadcast(this->Health / 100.f, this->Shield / 100.f, true);

		if (this->Health == 0.f && this->GetOwner())
		{
			Cast<ARagnarokProjectCharacter>(this->GetOwner())->Death();
		}
	}
}

void UPlayerHealthShieldComponent::Heal(float HealAmount)
{
	Super::Heal(HealAmount);
	this->OnUpdateHealthShield.Broadcast(this->Health / 100.f, this->Shield / 100.f, false);
}