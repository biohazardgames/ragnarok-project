// Fill out your copyright notice in the Description page of Project Settings.

#include "CamShake.h"

UCamShake::UCamShake()
{
	this->OscillationDuration = 0.2f;
	this->OscillationBlendInTime = 0.05f;
	this->OscillationBlendOutTime = 0.15f;
	this->RotOscillation.Pitch.Amplitude = 0.4f;
	this->RotOscillation.Pitch.Frequency = 20.f;
	this->RotOscillation.Yaw.Amplitude = 0.f;
	this->RotOscillation.Yaw.Frequency = 0.f;
	this->RotOscillation.Roll.Amplitude = 0.4f;
	this->RotOscillation.Roll.Frequency = 30.f;
	this->LocOscillation.X.Amplitude = 0.f;
	this->LocOscillation.X.Frequency = 0.f;
	this->LocOscillation.Y.Amplitude = 0.f;
	this->LocOscillation.Y.Frequency = 0.f;
	this->LocOscillation.Z.Amplitude = 0.f;
	this->LocOscillation.Z.Frequency = 0.f;
	this->FOVOscillation.Amplitude = 0.5f;
	this->FOVOscillation.Frequency = 20.f;
	this->AnimPlayRate = 1.f;
	this->AnimScale = 1.f;
	this->AnimBlendInTime = 0.2f;
	this->AnimBlendOutTime = 0.2f;
}