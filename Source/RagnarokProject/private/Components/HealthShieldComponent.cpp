#include "HealthShieldComponent.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "Blueprint/UserWidget.h"

// Sets default values for this component's properties
UHealthShieldComponent::UHealthShieldComponent()
{
	this->MaxHealth = 100.f;
	this->MaxShield = 100.f;	
	this->Health = this->MaxHealth;
	this->Shield = this->MaxShield;
}

// Called when the game starts
void UHealthShieldComponent::BeginPlay()
{
	Super::BeginPlay();

	if (this->GetOwner())
	{
		this->GetOwner()->OnTakeAnyDamage.AddDynamic(this, &UHealthShieldComponent::HandleTakeAnyDamage);
	}
}

void UHealthShieldComponent::HandleTakeAnyDamage(AActor* DamagedActor, float Damage, const class UDamageType* DamageType, class AController* InstigatedBy,
	AActor* DamageCauser)
{
	if (DamageCauser != DamagedActor && DamageCauser != this->GetOwner())
	{
		float RemainingDamage = NULL;
		
		if (Damage >= this->Shield)
		{
			RemainingDamage = Damage - this->Shield;
			this->Shield = 0.f;
		}
		else
		{
			RemainingDamage = 0.f;
			this->Shield = FMath::Clamp(this->Shield - Damage, 0.f, this->MaxShield);
		}

		if (this->Shield == 0.f && RemainingDamage > 0.f)
		{
			this->Health = (RemainingDamage >= this->Health) 
				? 0.f
				: FMath::Clamp(this->Health - RemainingDamage, 0.f, this->MaxHealth);
		}
	}
}

void UHealthShieldComponent::Heal(float HealAmount)
{
	this->Health = ((this->Health + HealAmount) >= this->MaxHealth)
			? this->MaxHealth
			: FMath::Clamp(this->Health + HealAmount, 0.f, this->MaxHealth);
}

float UHealthShieldComponent::GetMaxHealth()
{
	return this->MaxHealth;
}

float UHealthShieldComponent::GetHealth()
{
	return this->Health;
}

void UHealthShieldComponent::SetMaxShield(float Shield)
{
	this->MaxShield = Shield;
}

void UHealthShieldComponent::SetShield(float Shield)
{
	this->Shield = Shield;
}