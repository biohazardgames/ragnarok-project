// Fill out your copyright notice in the Description page of Project Settings.

#include "PurchasableTurret.h"
#include "EngineMinimal.h"

APurchasableTurret::APurchasableTurret() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/RagnarokProject/PurchasableObjects/Turret/Model/TurretBase.TurretBase'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(1.5f, 1.5f, 1.5f));
		this->Mesh->SetGenerateOverlapEvents(false);
		this->Mesh->SetupAttachment(this->RootComponent);
		this->Mesh->BodyInstance.bLockXRotation = true;
		this->Mesh->BodyInstance.bLockYRotation = true;
		this->Mesh->BodyInstance.bLockXTranslation = true;
		this->Mesh->BodyInstance.bLockYTranslation = true;
	}

	this->BarrelsMesh = CreateDefaultSubobject<UStaticMeshComponent>("BarrelsMesh");

	MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/RagnarokProject/PurchasableObjects/Turret/Model/TurretTop.TurretTop'"));

	if (MeshAsset.Succeeded())
	{
		this->BarrelsMesh->SetStaticMesh(MeshAsset.Object);
		FVector Location = GetActorLocation();
		this->BarrelsMesh->SetWorldLocation(FVector(Location.X, Location.Y, Location.Z + 85.f));
		this->BarrelsMesh->SetWorldScale3D(FVector(1.5f, 1.5f, 1.5f));
		this->BarrelsMesh->SetGenerateOverlapEvents(false);
		this->BarrelsMesh->SetupAttachment(RootComponent);
	}

	auto MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/RagnarokProject/PurchasableObjects/Model/MatPurchasableObject.MatPurchasableObject'"));
	if (MaterialAsset.Succeeded())
	{
		this->Material = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->Mesh);
		this->Mesh->SetMaterial(0, this->Material);

		this->BarrelsMaterial = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->BarrelsMesh);
		this->BarrelsMesh->SetMaterial(0, this->BarrelsMaterial);
	}

	this->InteractionRadius->InitSphereRadius(125.f);

	this->Cost = 500.f;
}

// Called when the game starts or when spawned
void APurchasableTurret::BeginPlay()
{
	Super::BeginPlay();
}

void APurchasableTurret::OnObjectPurchased(float Money)
{
	Super::OnObjectPurchased(Money);

	UMaterialInstanceDynamic* Material = (UMaterialInstanceDynamic*)this->BarrelsMesh->GetMaterial(0);
	FLinearColor Color = (Money >= this->Cost) ? FColor::Green : FColor::Red;
	Material->SetVectorParameterValue(FName(TEXT("Color")), Color);
}


