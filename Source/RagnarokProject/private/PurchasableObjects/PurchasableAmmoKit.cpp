#include "PurchasableAmmoKit.h"
#include "EngineMinimal.h"

APurchasableAmmoKit::APurchasableAmmoKit() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/RagnarokProject/PurchasableObjects/AmmoKit/AmmoKit.AmmoKit'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(300.f, 300.f, 300.f));
		this->Mesh->SetWorldRotation(FRotator(90.0f, 0.f, 0.0f));
		this->Mesh->SetGenerateOverlapEvents(false);
		this->Mesh->SetupAttachment(this->RootComponent);
		this->Mesh->BodyInstance.bLockXRotation = true;
		this->Mesh->BodyInstance.bLockYRotation = true;
		this->Mesh->BodyInstance.bLockXTranslation = true;
		this->Mesh->BodyInstance.bLockYTranslation = true;
	}

	this->InteractionRadius->InitSphereRadius(100.f);

	this->Cost = 50.f;
	this->EffectQuantity = 60;
}

// Called when the game starts or when spawned
void APurchasableAmmoKit::BeginPlay()
{
	Super::BeginPlay();
}

void APurchasableAmmoKit::OnObjectPurchased(float Money)
{
	Super::OnObjectPurchased(Money);
}

bool APurchasableAmmoKit::CheckSpecificPurchaseCondition()
{
	return (
			this->Manager->GetPlayer()->GetCurrentWeapon()->GetCurrentAmmo()
			<
			this->Manager->GetPlayer()->GetCurrentWeapon()->GetMaxAmmo()
			);
}


