#include "TurretProjectile.h"
#include "UObject/ConstructorHelpers.h"
#include "Components/StaticMeshComponent.h"
#include "GameFramework/ProjectileMovementComponent.h"
#include "Engine/StaticMesh.h"
#include "AutomaticTurret.h"
#include "BaseEnemy.h"

ATurretProjectile::ATurretProjectile() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/StarterContent/Shapes/Shape_Sphere.Shape_Sphere'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(0.5f, 0.2f, 0.15f));
	}

	auto MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/RagnarokProject/PurchasableObjects/Turret/Model/MatTurretProjectile.MatTurretProjectile'"));
	if (MaterialAsset.Succeeded())
	{
		this->Material = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->Mesh);
		this->Mesh->SetMaterial(0, this->Material);
	}
}

void ATurretProjectile::BeginPlay()
{
	Super::BeginPlay();
}

void ATurretProjectile::OnHit(UPrimitiveComponent* HitComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, FVector NormalImpulse, const FHitResult& Hit)
{
	Super::OnHit(HitComp, OtherActor, OtherComp, NormalImpulse, Hit);

	if (OtherActor && OtherActor->GetClass()->IsChildOf<ABaseEnemy>())
	{
		FPointDamageEvent DmgEvent;
		DmgEvent.Damage = Cast<AAutomaticTurret>(this->GetOwner())->GetDamage();
		Cast<ABaseEnemy>(OtherActor)->TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);
	}

	this->Destroy();
}