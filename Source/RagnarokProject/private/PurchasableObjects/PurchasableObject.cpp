// Fill out your copyright notice in the Description page of Project Settings.

#include "PurchasableObject.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "Components/InputComponent.h"
#include "Kismet/GameplayStatics.h"
#include "Engine/CollisionProfile.h"
#include "RagnarokProject.h"

// Sets default values
APurchasableObject::APurchasableObject()
{
	PrimaryActorTick.bCanEverTick = false;

	this->Mesh = CreateDefaultSubobject<UStaticMeshComponent>("Mesh");
	this->Mesh->SetGenerateOverlapEvents(false);
	this->Mesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	this->Mesh->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
	this->Mesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);

	this->InteractionRadius = CreateDefaultSubobject<USphereComponent>(TEXT("CollisionSphere"));
	this->InteractionRadius->SetCollisionObjectType(COLLISION_TRIGGER_COMPONENT);
	this->InteractionRadius->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	this->InteractionRadius->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->InteractionRadius->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
	this->RootComponent = this->InteractionRadius;

	this->InteractionRadius->SetGenerateOverlapEvents(true);
	this->InteractionRadius->OnComponentBeginOverlap.AddDynamic(this, &APurchasableObject::OnOverlapBegin);
	this->InteractionRadius->OnComponentEndOverlap.AddDynamic(this, &APurchasableObject::OnOverlapEnd);

	this->Material = NULL;
	
	auto Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/PurchasableObjects/Sounds/PurchasedObjectSound.PurchasedObjectSound'"));

	if (Audio.Succeeded())
	{
		this->PurchasedSound = Audio.Object;
	}
	
	Audio = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/PurchasableObjects/Sounds/NotEnoughMoneySound.NotEnoughMoneySound'"));

	if (Audio.Succeeded())
	{
		this->NotEnoughMoneySound = Audio.Object;
	}
}

// Called when the game starts or when spawned
void APurchasableObject::BeginPlay()
{
	Super::BeginPlay();

	TArray<AActor*> Managers;
	UGameplayStatics::GetAllActorsOfClass(GetWorld(), APurchasableObjectsManager::StaticClass(), Managers);
	this->Manager = Cast<APurchasableObjectsManager>(Managers[0]);
	this->Manager->OnObjectPurchased.AddDynamic(this, &APurchasableObject::OnObjectPurchased);

	if (this->Material)
	{
		UMaterialInstanceDynamic* Material = (UMaterialInstanceDynamic*)this->Mesh->GetMaterial(0);
		Material->SetVectorParameterValue(FName(TEXT("Color")), FColor::Red);
	}
}

void APurchasableObject::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor && OtherActor != this && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		this->OnNearPlayer(this->Cost);
		Cast<ARagnarokProjectCharacter>(OtherActor)->SetPurchasableObject(this);
	}
}

void APurchasableObject::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex) {

	if (OtherActor && OtherActor != this && OtherActor->IsA(ARagnarokProjectCharacter::StaticClass()))
	{
		this->OnNearPlayer(-1);
		Cast<ARagnarokProjectCharacter>(OtherActor)->SetPurchasableObject(NULL);
	}
}

float APurchasableObject::PurchaseObject(float Money)
{
	if (Money >= this->Cost && this->CheckSpecificPurchaseCondition())
	{
		this->PlaySound(this->PurchasedSound);
		this->Manager->ObjectPurchased(this, Money - this->Cost);
		return -1.f * this->Cost;
	}

	this->PlaySound(this->NotEnoughMoneySound);
	return 0.f;
}

bool APurchasableObject::CheckSpecificPurchaseCondition()
{
	return true;
}

void APurchasableObject::OnObjectPurchased(float Money)
{
	if (this->Material)
	{
		UMaterialInstanceDynamic* Material = (UMaterialInstanceDynamic*)this->Mesh->GetMaterial(0);
		FLinearColor Color = (Money >= this->Cost) ? FColor::Green : FColor::Red;
		Material->SetVectorParameterValue(FName(TEXT("Color")), Color);
	}
}

float APurchasableObject::GetEffectQuantity()
{
	return this->EffectQuantity;
}

void APurchasableObject::PlaySound(USoundBase* Sound)
{
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation
		(
			this,
			Sound,
			this->GetActorLocation()
		);
	}
}

