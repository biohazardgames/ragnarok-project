// Fill out your copyright notice in the Description page of Project Settings.

#include "AutomaticTurret.h"
#include "EngineUtils.h"
#include "EngineMinimal.h"
#include "Kismet/GameplayStatics.h"
#include "Sound/SoundBase.h"
#include "TimerManager.h"
#include "RagnarokProjectCharacter.h"
#include "Engine/CollisionProfile.h"
#include "TurretProjectile.h"
#include "BaseEnemy.h"

AAutomaticTurret::AAutomaticTurret()
{
	PrimaryActorTick.bCanEverTick = true;

	this->FireRadius = CreateDefaultSubobject<USphereComponent>(TEXT("FireRadius"));
	RootComponent = this->FireRadius;
	this->FireRadius->SetCollisionObjectType(COLLISION_TRIGGER_COMPONENT);
	this->FireRadius->SetGenerateOverlapEvents(true);
	this->FireRadius->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
	this->FireRadius->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	this->FireRadius->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Overlap);
	this->FireRadius->InitSphereRadius(1000.f);
	this->FireRadius->OnComponentBeginOverlap.AddDynamic(this, &AAutomaticTurret::OnOverlapBegin);
	this->FireRadius->OnComponentEndOverlap.AddDynamic(this, &AAutomaticTurret::OnOverlapEnd);

	this->BaseMesh = CreateDefaultSubobject<UStaticMeshComponent>("BaseMesh");

	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/RagnarokProject/PurchasableObjects/Turret/Model/TurretBase.TurretBase'"));

	if (MeshAsset.Succeeded())
	{
		this->BaseMesh->SetStaticMesh(MeshAsset.Object);
		this->BaseMesh->SetWorldScale3D(FVector(1.5f, 1.5f, 1.5f));
		this->BaseMesh->SetGenerateOverlapEvents(false);
		this->BaseMesh->SetupAttachment(RootComponent);
		this->BaseMesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
		this->BaseMesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
		this->BaseMesh->SetCollisionResponseToChannel(COLLISION_ENEMY, ECR_Ignore);
	}

	this->BarrelsMesh = CreateDefaultSubobject<UStaticMeshComponent>("BarrelsMesh");

	MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/RagnarokProject/PurchasableObjects/Turret/Model/TurretTop.TurretTop'"));

	if (MeshAsset.Succeeded())
	{
		this->BarrelsMesh->SetStaticMesh(MeshAsset.Object);
		FVector Location = GetActorLocation();
		this->BarrelsMesh->SetWorldLocation(FVector(Location.X, Location.Y, Location.Z + 85.f));
		this->BarrelsMesh->SetWorldScale3D(FVector(1.5f, 1.5f, 1.5f));
		this->BarrelsMesh->SetGenerateOverlapEvents(false);
		this->BarrelsMesh->SetupAttachment(RootComponent);
		this->BarrelsMesh->BodyInstance.SetCollisionProfileName("Turret");
		this->BarrelsMesh->SetCollisionResponseToChannel(COLLISION_WEAPON, ECR_Ignore);
		this->BarrelsMesh->SetCollisionResponseToChannel(ECC_Camera, ECR_Ignore);
	}

	auto MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/RagnarokProject/PurchasableObjects/Turret/Model/MatAutomaticTurret.MatAutomaticTurret'"));
	if (MaterialAsset.Succeeded())
	{
		this->BaseMaterial = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->BaseMesh);
		this->BaseMesh->SetMaterial(0, this->BaseMaterial);

		this->BarrelsMaterial = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->BarrelsMesh);
		this->BarrelsMesh->SetMaterial(0, this->BarrelsMaterial);
	}

	auto SoundAsset = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/PurchasableObjects/Turret/Sounds/Fire.Fire'"));
	this->FireSound = SoundAsset.Object;

	SoundAsset = ConstructorHelpers::FObjectFinder<USoundBase>(TEXT("SoundWave'/Game/RagnarokProject/PurchasableObjects/Turret/Sounds/Armed.Armed'"));
	this->ArmedSound = SoundAsset.Object;

	this->Damage = 20.f;
	this->FireRate = 1.f;
	this->CanFire = false;
}

void AAutomaticTurret::BeginPlay()
{
	Super::BeginPlay();

	if (this->ArmedSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, this->ArmedSound, this->GetActorLocation());
	}
}

void AAutomaticTurret::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	if (this->Targets.Num() > 0)
	{
		FVector FireDirection = this->Targets[0]->GetActorLocation() - this->GetActorLocation();
		FRotator TurretRotation = FRotationMatrix::MakeFromX(FVector(FireDirection.X, FireDirection.Y, 0.0f)).Rotator();
		this->BarrelsMesh->SetRelativeRotation(TurretRotation, false, nullptr, ETeleportType::None);
	}
}

void AAutomaticTurret::Fire()
{
	if (this->CanFire)
	{
		this->LaunchProjectile(FName(TEXT("Barrel1")));
		this->LaunchProjectile(FName(TEXT("Barrel2")));
	}
}

void AAutomaticTurret::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult) {

	if (OtherActor && OtherActor != this && OtherActor->GetClass()->IsChildOf<ABaseEnemy>())
	{
		if (this->Targets.Num() == 0)
		{
			this->CanFire = true;
			this->GetWorld()->GetTimerManager().SetTimer
			(
				this->FireTimer,
				this,
				&AAutomaticTurret::Fire,
				this->FireRate,
				true
			);
		}

		this->Targets.Emplace(OtherActor);
	}
}

void AAutomaticTurret::OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex) {
	
	if (OtherActor && OtherActor != this && OtherActor->GetClass()->IsChildOf<ABaseEnemy>())
	{
		this->DeleteTarget(OtherActor->GetUniqueID());
	}
}

float AAutomaticTurret::GetDamage() 
{
	return this->Damage;
}

void AAutomaticTurret::LaunchProjectile(FName SocketName)
{
	ATurretProjectile* Projectile = this->GetWorld()->SpawnActor<ATurretProjectile>
			(
				ATurretProjectile::StaticClass(),
				this->BarrelsMesh->GetSocketLocation(SocketName),
				this->BarrelsMesh->GetSocketRotation(SocketName)
			);

	this->PlaySound(this->FireSound);
	Projectile->SetOwner(this);
}

void AAutomaticTurret::PlaySound(USoundBase* Sound)
{
	if (Sound)
	{
		UGameplayStatics::PlaySoundAtLocation
		(
			this,
			Sound,
			this->GetActorLocation()
		);
	}
}

void AAutomaticTurret::TargetKilled(uint32 TargetId)
{
	this->DeleteTarget(TargetId);
}

void AAutomaticTurret::DeleteTarget(uint32 TargetId)
{
	this->CanFire = false;

	int32 Index = this->Targets.IndexOfByPredicate([TargetId](const AActor* SearchActor) {
		return SearchActor->GetUniqueID() == TargetId;
	});

	if (Index != INDEX_NONE)
	{
		this->Targets.RemoveAt(Index);
	}

	if (this->Targets.Num() == 0)
	{
		this->GetWorld()->GetTimerManager().ClearTimer(this->FireTimer);
	}
	else
	{
		this->CanFire = true;
	}
}