#include "PurchasableMedicalKit.h"
#include "EngineMinimal.h"

APurchasableMedicalKit::APurchasableMedicalKit() : Super()
{
	auto MeshAsset = ConstructorHelpers::FObjectFinder<UStaticMesh>(TEXT("StaticMesh'/Game/RagnarokProject/PurchasableObjects/MedicalKit/MedicalKit.MedicalKit'"));

	if (MeshAsset.Succeeded())
	{
		this->Mesh->SetStaticMesh(MeshAsset.Object);
		this->Mesh->SetWorldScale3D(FVector(40.f, 40.f, 40.f));
		this->Mesh->SetWorldRotation(FRotator(90.0f, 0.f, 0.0f));
		this->Mesh->SetGenerateOverlapEvents(false);
		this->Mesh->SetupAttachment(this->RootComponent);
		this->Mesh->BodyInstance.bLockXRotation = true;
		this->Mesh->BodyInstance.bLockYRotation = true;
		this->Mesh->BodyInstance.bLockXTranslation = true;
		this->Mesh->BodyInstance.bLockYTranslation = true;
	}

	this->InteractionRadius->InitSphereRadius(100.f);

	this->Cost = 100.f;
	this->EffectQuantity = 20;
}

// Called when the game starts or when spawned
void APurchasableMedicalKit::BeginPlay()
{
	Super::BeginPlay();
}

void APurchasableMedicalKit::OnObjectPurchased(float Money)
{
	Super::OnObjectPurchased(Money);
}

bool APurchasableMedicalKit::CheckSpecificPurchaseCondition()
{
	return (
		this->Manager->GetPlayer()->GetHealthShieldComponent()->GetHealth()
		<
		this->Manager->GetPlayer()->GetHealthShieldComponent()->GetMaxHealth()
		);
}


