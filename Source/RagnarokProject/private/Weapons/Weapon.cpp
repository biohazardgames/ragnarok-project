// Fill out your copyright notice in the Description page of Project Settings.

#include "Weapon.h"
#include "PhysicalMaterials/PhysicalMaterial.h"
#include "Particles/ParticleSystemComponent.h"
#include "Kismet/GameplayStatics.h"
#include "BaseEnemy.h"
#include "RagnarokProject.h"

// Sets default values
AWeapon::AWeapon()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	this->PrimaryActorTick.bCanEverTick = false;

	this->Mesh = CreateDefaultSubobject<USkeletalMeshComponent>(TEXT("WeaponMesh"));
	this->Mesh->CastShadow = true;	
	this->Mesh->SetCollisionEnabled(ECollisionEnabled::NoCollision);
	this->Mesh->SetCollisionResponseToAllChannels(ECR_Ignore);
	this->Mesh->BodyInstance.bLockXRotation = true;
	this->Mesh->BodyInstance.bLockYRotation = true;
	this->Mesh->BodyInstance.bLockZTranslation = true;
	this->Mesh->SetGenerateOverlapEvents(false);
	this->Mesh->SetNotifyRigidBodyCollision(false);

	this->RootComponent = this->Mesh;

	this->LastFireTime = 0.f;
	this->TracerTargetName = "Target";
	this->FireCamShake = UCamShake::StaticClass();

	auto ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Weapons/AssaultRifle/Effects/Impact/GenericImpact/P_RifleImpact.P_RifleImpact'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->DefaultImpactEffect = ParticleSystemAsset.Object;
	}

	ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Weapons/AssaultRifle/Effects/Impact/BloodImpact/P_blood_splash_02.P_blood_splash_02'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->EnemyImpactEffect = ParticleSystemAsset.Object;
	}
}

// Called when the game starts or when spawned
void AWeapon::BeginPlay()
{
	Super::BeginPlay();
}

void AWeapon::SetOwner(AActor* NewOwner)
{
	Super::SetOwner(NewOwner);
	this->Player = Cast<ARagnarokProjectCharacter>(NewOwner);
}

void AWeapon::StartFire()
{
	this->GetWorldTimerManager().SetTimer
	(
		this->FireTimer,
		this,
		&AWeapon::Fire,
		this->TimeBetweenShots,
		true,
		FMath::Max(this->LastFireTime + this->TimeBetweenShots - this->GetWorld()->TimeSeconds, 0.f)
	);
}

void AWeapon::Fire()
{
	if (this->GetOwner() && this->AmmoInClip > 0)
	{
		FVector EyeLocation;
		FRotator EyeRotation;
		this->GetOwner()->GetActorEyesViewPoint(EyeLocation, EyeRotation);

		FVector ShotDirection = EyeRotation.Vector();

		// Bullet Spread
		float HalfRad = FMath::DegreesToRadians(this->BulletSpread);
		ShotDirection = FMath::VRandCone(ShotDirection, HalfRad, HalfRad);

		FVector TraceEnd = EyeLocation + (ShotDirection * this->MaxShotDistance);

		FCollisionQueryParams QueryParams;
		QueryParams.AddIgnoredActor(this->GetOwner());
		QueryParams.AddIgnoredActor(this);
		QueryParams.bTraceComplex = true;
		QueryParams.bReturnPhysicalMaterial = true;

		FVector TracerEndPoint = TraceEnd;

		FHitResult Hit;
		if (this->GetWorld()->LineTraceSingleByChannel(Hit, EyeLocation, TraceEnd, COLLISION_WEAPON, QueryParams))
		{
			if (Hit.GetActor()->GetClass()->IsChildOf<ABaseEnemy>())
			{
				FPointDamageEvent DmgEvent;
				DmgEvent.Damage = this->Damage;
				Cast<ABaseEnemy>(Hit.GetActor())->TakeDamage(DmgEvent.Damage, DmgEvent, NULL, this);
			}
			this->PlayImpactEffects(Hit);

			TracerEndPoint = Hit.ImpactPoint;
		}	

		this->PlayFireEffects(TracerEndPoint);
		this->LastFireTime = this->GetWorld()->TimeSeconds;

		this->PlayWeaponSound(this->FireSound);

		this->UpdateAmmo(-1, 0);
	}
	else
	{
		this->StopFire();
	}
}

void AWeapon::StopFire()
{
	this->GetWorldTimerManager().ClearTimer(this->FireTimer);
}

void AWeapon::StartReload()
{
	if (this->AmmoInClip < this->ClipSize && this->CurrentAmmo > 0)
	{
		this->GetWorldTimerManager().SetTimer
		(
			this->ReloadTimer,
			this,
			&AWeapon::ReloadFinished,
			this->PlayWeaponAnimation(this->ReloadAnimation),
			false
		);
		this->PlayWeaponSound(this->ReloadSound);
	}
	else
	{
		this->Player->SetCanFire(true);
		this->Player->SetCanReload(true);
		this->Player->SetCanAim(true);
		this->Player->SetCanSwapWeapons(true);
	}	
}

void AWeapon::ReloadFinished()
{
	this->StopWeaponAnimation(this->ReloadAnimation);
	this->GetWorld()->GetTimerManager().ClearTimer(this->ReloadTimer);
	this->Player->SetCanFire(true);
	this->Player->SetCanReload(true);
	this->Player->SetCanAim(true);
	this->Player->SetCanSwapWeapons(true);

	int AmmoToFill = ((this->ClipSize - this->AmmoInClip) < this->CurrentAmmo)
		? this->ClipSize - this->AmmoInClip
		: this->CurrentAmmo;

	this->UpdateAmmo(AmmoToFill, -1 * AmmoToFill);
}

void AWeapon::PlayWeaponSound(USoundCue* SoundToPlay)
{
	if (this->GetOwner() && SoundToPlay)
	{
		UGameplayStatics::SpawnSoundAttached(SoundToPlay, this->GetOwner()->GetRootComponent());
	}
}

float AWeapon::PlayWeaponAnimation(UAnimMontage* Animation, float InPlayRate, FName StartSectionName)
{
	return (this->GetOwner() && Animation) 
		? this->Player->PlayAnimMontage(Animation, InPlayRate, StartSectionName) 
		: 0.f;
}


void AWeapon::StopWeaponAnimation(UAnimMontage* Animation)
{
	if (this->GetOwner() && Animation)
	{
		this->Player->StopAnimMontage(Animation);
	}
}

void AWeapon::PlayFireEffects(FVector TraceEnd)
{
	if (this->MuzzleFX)
	{
		UGameplayStatics::SpawnEmitterAttached(this->MuzzleFX, this->Mesh, this->MuzzleAttachSocketName);
	}

	if (this->TracerEffect)
	{
		UParticleSystemComponent* TracerComp = UGameplayStatics::SpawnEmitterAtLocation
		(
			this->GetWorld(),
			this->TracerEffect,
			this->Mesh->GetSocketLocation(this->MuzzleAttachSocketName)
		);

		if (TracerComp)
		{
			TracerComp->SetVectorParameter(this->TracerTargetName, TraceEnd);
		}
	}

	if (this->GetOwner())
	{
		APlayerController* PC = Cast<APlayerController>(Cast<APawn>(this->GetOwner())->GetController());
		if (PC)
		{
			PC->ClientPlayCameraShake(this->FireCamShake);
		}
	}
}

void AWeapon::PlayImpactEffects(FHitResult Hit)
{
	UParticleSystem* SurfaceEffect = (Hit.GetActor()->GetClass()->IsChildOf<ABaseEnemy>()) ? this->EnemyImpactEffect : DefaultImpactEffect;

	if (SurfaceEffect)
	{
		FVector ShotDirection = Hit.ImpactPoint - this->Mesh->GetSocketLocation(this->MuzzleAttachSocketName);
		ShotDirection.Normalize();

		UGameplayStatics::SpawnEmitterAtLocation
		(
			this->GetWorld(),
			SurfaceEffect,
			Hit.ImpactPoint,
			ShotDirection.Rotation()
		);
	}
}

int AWeapon::GetAmmoInClip()
{
	return this->AmmoInClip;
}

int AWeapon::GetCurrentAmmo()
{
	return this->CurrentAmmo;
}

int AWeapon::GetMaxAmmo()
{
	return this->MaxAmmo;
}

void AWeapon::UpdateAmmo(int ClipAmmo, int Ammo)
{
	this->CurrentAmmo = ((this->CurrentAmmo + Ammo) >= this->MaxAmmo)
		? this->MaxAmmo
		: this->CurrentAmmo + Ammo;

	this->Player->AmmoUpdated
			(
				this->AmmoInClip += ClipAmmo,
				this->CurrentAmmo
			);
}

void AWeapon::SetBulletSpreadByPlayerPosition(FString Position)
{
	this->BulletSpread = this->BulletSpreadByPlayerPosition[Position];
}