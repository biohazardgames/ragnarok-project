// Fill out your copyright notice in the Description page of Project Settings.

#include "LaserCannon.h"

// Sets default values
ALaserCannon::ALaserCannon() : Super()
{
	this->ClipSize = 10;
	this->MaxAmmo = 90;
	this->AmmoInClip = this->ClipSize;
	this->CurrentAmmo = this->MaxAmmo;
	this->Damage = 40.f;
	this->TimeBetweenShots = 1.31f;
	this->MaxShotDistance = 10000.f;

	this->BulletSpreadByPlayerPosition.Emplace("Standing", 2.5f);
	this->BulletSpreadByPlayerPosition.Emplace("Crouching", 1.5f);
	this->BulletSpread = this->BulletSpreadByPlayerPosition["Standing"];

	auto AnimationMontage = ConstructorHelpers::FObjectFinder<UAnimMontage>(TEXT("AnimMontage'/Game/RagnarokProject/Player/Soldier/Animations/Reload_Shotgun_Hip_Montage.Reload_Shotgun_Hip_Montage'"));
	if (AnimationMontage.Succeeded())
	{
		this->ReloadAnimation = AnimationMontage.Object;
	}

	auto ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Weapons/AssaultRifle/Effects/Muzzle/P_AssaultRifle_MF.P_AssaultRifle_MF'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->MuzzleFX = ParticleSystemAsset.Object;
	}

	ParticleSystemAsset = ConstructorHelpers::FObjectFinder<UParticleSystem>(TEXT("ParticleSystem'/Game/RagnarokProject/Weapons/LaserCannon/Effects/Tracer/P_LaserTrail.P_LaserTrail'"));
	if (ParticleSystemAsset.Succeeded())
	{
		this->TracerEffect = ParticleSystemAsset.Object;
	}
	
	this->MuzzleAttachSocketName = "LaserCannonMuzzleSocket";	


	static ConstructorHelpers::FObjectFinder<USkeletalMesh> WeaponMesh(TEXT("SkeletalMesh'/Game/RagnarokProject/Weapons/LaserCannon/SK_LaserCannon.SK_LaserCannon'"));
	if (WeaponMesh.Succeeded())
	{
		this->Mesh->SetSkeletalMesh(WeaponMesh.Object);
		this->Mesh->SetWorldScale3D(FVector(1.f, 1.f, 1.f));
	}

	this->RootComponent = this->Mesh;

	auto MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/RagnarokProject/Weapons/LaserCannon/Materials/Material0'"));
	if (MaterialAsset.Succeeded())
	{
		this->Material0 = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->Mesh);
		this->Mesh->SetMaterial(0, Material0);
	}

	MaterialAsset = ConstructorHelpers::FObjectFinder<UMaterial>(TEXT("Material'/Game/RagnarokProject/Weapons/LaserCannon/Materials/Material1'"));
	if (MaterialAsset.Succeeded())
	{
		this->Material1 = UMaterialInstanceDynamic::Create(MaterialAsset.Object, this->Mesh);
		this->Mesh->SetMaterial(1, Material1);
	}

	auto SoundAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/RagnarokProject/Weapons/LaserCannon/Sounds/Fire.Fire'"));
	if (SoundAsset.Succeeded())
	{
		this->FireSound = SoundAsset.Object;
	}

	SoundAsset = ConstructorHelpers::FObjectFinder<USoundCue>(TEXT("SoundCue'/Game/RagnarokProject/Weapons/AssaultRifle/Sounds/Reload.Reload'"));
	if (SoundAsset.Succeeded())
	{
		this->ReloadSound = SoundAsset.Object;
	}
}

// Called when the game starts or when spawned
void ALaserCannon::BeginPlay()
{
	Super::BeginPlay();

}
