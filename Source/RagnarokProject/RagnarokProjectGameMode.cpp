// Copyright 1998-2018 Epic Games, Inc. All Rights Reserved.

#include "RagnarokProjectGameMode.h"
#include "RagnarokProjectCharacter.h"
#include "UObject/ConstructorHelpers.h"

ARagnarokProjectGameMode::ARagnarokProjectGameMode()
{
	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/RagnarokProject/Player/Soldier/BPRPCharacter"));
	if (PlayerPawnBPClass.Class)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}
